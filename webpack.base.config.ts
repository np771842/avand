import { Configuration } from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import DotEnv from 'dotenv-webpack';
import ESLintPlugin from 'eslint-webpack-plugin';
import { InjectManifest } from 'workbox-webpack-plugin';

const config: Configuration = {
  entry: path.resolve(__dirname, 'frontend/src/index.tsx'),
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.(ts|js)x?$/i,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript']
          }
        }
      },
      {
        exclude: /node_modules/,
        test: /\.s[ac]ss$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpe?g|gif|png|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'frontend/public/index.html')
    }),
    new ForkTsCheckerWebpackPlugin({
      async: false
    }),
    new ESLintPlugin({
      extensions: ['js', 'jsx', 'ts', 'tsx']
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'frontend/src/static'),
          to: 'static'
        }
      ]
    }),
    new DotEnv({
      path: process.env.ENV === 'development' ? '.env' : '.env.production'
    }),
    new InjectManifest({
      swDest: 'sw.js',
      swSrc: path.resolve(__dirname, 'frontend/src/sw/sw.js')
    })
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.css', '.scss'],
    modules: ['node_modules', path.join(__dirname, 'frontend')],
    plugins: [new TsconfigPathsPlugin()]
  }
};

export default config;
