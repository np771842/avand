export const planetWookieeMock = {
  oaoohuwhao: 60,
  whwokao:
    'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/?akrarrwo=2&wwoorcscraao=ohooooorahwowo',
  akrcwohoahoohuc: null,
  rcwochuanaoc: [
    {
      whrascwo: 'Traaoooooahwhwo',
      rcooaoraaoahoowh_akworcahoowa: '23',
      oorcrhahaoraan_akworcahoowa: '304',
      waahrascwoaoworc: '10465',
      oaanahscraaowo: 'rarcahwa',
      rrrcrahoahaoro: '1 caorawhwararcwa',
      aoworcrcraahwh: 'wawocworcao',
      churcwwraoawo_ohraaoworc: '1',
      akooakhuanraaoahoowh: '200000',
      rcwocahwawowhaoc: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/6/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/7/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/8/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/9/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/11/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/43/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/62/'
      ],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      oarcworaaowowa: '2014-12-09T13:50:49.641000Z',
      wowaahaowowa: '2014-12-20T20:58:18.411000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/1/'
    },
    {
      whrascwo: 'Aanwaworcrarawh',
      rcooaoraaoahoowh_akworcahoowa: '24',
      oorcrhahaoraan_akworcahoowa: '364',
      waahrascwoaoworc: '12500',
      oaanahscraaowo: 'aowoscakworcraaowo',
      rrrcrahoahaoro: '1 caorawhwararcwa',
      aoworcrcraahwh: 'rrrcraccanrawhwac, scoohuwhaoraahwhc',
      churcwwraoawo_ohraaoworc: '40',
      akooakhuanraaoahoowh: '2000000000',
      rcwocahwawowhaoc: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/68/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/81/'
      ],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      oarcworaaowowa: '2014-12-10T11:35:48.479000Z',
      wowaahaowowa: '2014-12-20T20:58:18.420000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/2/'
    },
    {
      whrascwo: 'Yrahoahwh IV',
      rcooaoraaoahoowh_akworcahoowa: '24',
      oorcrhahaoraan_akworcahoowa: '4818',
      waahrascwoaoworc: '10200',
      oaanahscraaowo: 'aowoscakworcraaowo, aorcooakahoaraan',
      rrrcrahoahaoro: '1 caorawhwararcwa',
      aoworcrcraahwh: 'shhuwhrranwo, rcraahwhwwoorcwocaoc',
      churcwwraoawo_ohraaoworc: '8',
      akooakhuanraaoahoowh: '1000',
      rcwocahwawowhaoc: [],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/'],
      oarcworaaowowa: '2014-12-10T11:37:19.144000Z',
      wowaahaowowa: '2014-12-20T20:58:18.421000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/3/'
    },
    {
      whrascwo: 'Hooaoac',
      rcooaoraaoahoowh_akworcahoowa: '23',
      oorcrhahaoraan_akworcahoowa: '549',
      waahrascwoaoworc: '7200',
      oaanahscraaowo: 'wwrcooufwowh',
      rrrcrahoahaoro: '1.1 caorawhwararcwa',
      aoworcrcraahwh: 'aohuwhwarcra, ahoawo oarahowoc, scoohuwhaoraahwh rcrawhrrwoc',
      churcwwraoawo_ohraaoworc: '100',
      akooakhuanraaoahoowh: 'huwhorwhooohwh',
      rcwocahwawowhaoc: [],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      oarcworaaowowa: '2014-12-10T11:39:13.934000Z',
      wowaahaowowa: '2014-12-20T20:58:18.423000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/4/'
    },
    {
      whrascwo: 'Drarroorhraac',
      rcooaoraaoahoowh_akworcahoowa: '23',
      oorcrhahaoraan_akworcahoowa: '341',
      waahrascwoaoworc: '8900',
      oaanahscraaowo: 'schurcorro',
      rrrcrahoahaoro: 'N/A',
      aoworcrcraahwh: 'cohrascak, shhuwhrranwoc',
      churcwwraoawo_ohraaoworc: '8',
      akooakhuanraaoahoowh: 'huwhorwhooohwh',
      rcwocahwawowhaoc: [],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      oarcworaaowowa: '2014-12-10T11:42:22.590000Z',
      wowaahaowowa: '2014-12-20T20:58:18.425000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/5/'
    },
    {
      whrascwo: 'Bwocakahwh',
      rcooaoraaoahoowh_akworcahoowa: '12',
      oorcrhahaoraan_akworcahoowa: '5110',
      waahrascwoaoworc: '118000',
      oaanahscraaowo: 'aowoscakworcraaowo',
      rrrcrahoahaoro: '1.5 (churcwwraoawo), 1 caorawhwararcwa (Canoohuwa Cahaoro)',
      aoworcrcraahwh: 'rrrac rrahrawhao',
      churcwwraoawo_ohraaoworc: '0',
      akooakhuanraaoahoowh: '6000000',
      rcwocahwawowhaoc: ['acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/26/'],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      oarcworaaowowa: '2014-12-10T11:43:55.240000Z',
      wowaahaowowa: '2014-12-20T20:58:18.427000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/6/'
    },
    {
      whrascwo: 'Ewhwaoorc',
      rcooaoraaoahoowh_akworcahoowa: '18',
      oorcrhahaoraan_akworcahoowa: '402',
      waahrascwoaoworc: '4900',
      oaanahscraaowo: 'aowoscakworcraaowo',
      rrrcrahoahaoro: '0.85 caorawhwararcwa',
      aoworcrcraahwh: 'wwoorcwocaoc, scoohuwhaoraahwhc, anraorwoc',
      churcwwraoawo_ohraaoworc: '8',
      akooakhuanraaoahoowh: '30000000',
      rcwocahwawowhaoc: ['acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/30/'],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'],
      oarcworaaowowa: '2014-12-10T11:50:29.349000Z',
      wowaahaowowa: '2014-12-20T20:58:18.429000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/7/'
    },
    {
      whrascwo: 'Nrarhoooo',
      rcooaoraaoahoowh_akworcahoowa: '26',
      oorcrhahaoraan_akworcahoowa: '312',
      waahrascwoaoworc: '12120',
      oaanahscraaowo: 'aowoscakworcraaowo',
      rrrcrahoahaoro: '1 caorawhwararcwa',
      aoworcrcraahwh: 'rrrcraccro acahananc, cohrascakc, wwoorcwocaoc, scoohuwhaoraahwhc',
      churcwwraoawo_ohraaoworc: '12',
      akooakhuanraaoahoowh: '4500000000',
      rcwocahwawowhaoc: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/21/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/35/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/36/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/37/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/38/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/39/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/42/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/60/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/61/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/66/'
      ],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      oarcworaaowowa: '2014-12-10T11:52:31.066000Z',
      wowaahaowowa: '2014-12-20T20:58:18.430000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/8/'
    },
    {
      whrascwo: 'Coorchucoarawhao',
      rcooaoraaoahoowh_akworcahoowa: '24',
      oorcrhahaoraan_akworcahoowa: '368',
      waahrascwoaoworc: '12240',
      oaanahscraaowo: 'aowoscakworcraaowo',
      rrrcrahoahaoro: '1 caorawhwararcwa',
      aoworcrcraahwh: 'oaahaorocoaraakwo, scoohuwhaoraahwhc',
      churcwwraoawo_ohraaoworc: 'huwhorwhooohwh',
      akooakhuanraaoahoowh: '1000000000000',
      rcwocahwawowhaoc: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/34/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/55/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/74/'
      ],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      oarcworaaowowa: '2014-12-10T11:54:13.921000Z',
      wowaahaowowa: '2014-12-20T20:58:18.432000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/9/'
    },
    {
      whrascwo: 'Krascahwhoo',
      rcooaoraaoahoowh_akworcahoowa: '27',
      oorcrhahaoraan_akworcahoowa: '463',
      waahrascwoaoworc: '19720',
      oaanahscraaowo: 'aowoscakworcraaowo',
      rrrcrahoahaoro: '1 caorawhwararcwa',
      aoworcrcraahwh: 'oooaworawh',
      churcwwraoawo_ohraaoworc: '100',
      akooakhuanraaoahoowh: '1000000000',
      rcwocahwawowhaoc: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/22/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/72/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/73/'
      ],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/'],
      oarcworaaowowa: '2014-12-10T12:45:06.577000Z',
      wowaahaowowa: '2014-12-20T20:58:18.434000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/10/'
    }
  ]
};
export const planetWookieeTransformedMock = {
  count: 60,
  next: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/?akrarrwo=2&wwoorcscraao=ohooooorahwowo',
  previous: null,
  results: [
    {
      name: 'Traaoooooahwhwo',
      rotation_period: '23',
      orbital_period: '304',
      diameter: '10465',
      climate: 'rarcahwa',
      gravity: '1 caorawhwararcwa',
      terrain: 'wawocworcao',
      surface_water: '1',
      population: '200000',
      residents: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/6/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/7/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/8/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/9/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/11/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/43/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/62/'
      ],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      created: '2014-12-09T13:50:49.641000Z',
      edited: '2014-12-20T20:58:18.411000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/1/'
    },
    {
      name: 'Aanwaworcrarawh',
      rotation_period: '24',
      orbital_period: '364',
      diameter: '12500',
      climate: 'aowoscakworcraaowo',
      gravity: '1 caorawhwararcwa',
      terrain: 'rrrcraccanrawhwac, scoohuwhaoraahwhc',
      surface_water: '40',
      population: '2000000000',
      residents: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/68/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/81/'
      ],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      created: '2014-12-10T11:35:48.479000Z',
      edited: '2014-12-20T20:58:18.420000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/2/'
    },
    {
      name: 'Yrahoahwh IV',
      rotation_period: '24',
      orbital_period: '4818',
      diameter: '10200',
      climate: 'aowoscakworcraaowo, aorcooakahoaraan',
      gravity: '1 caorawhwararcwa',
      terrain: 'shhuwhrranwo, rcraahwhwwoorcwocaoc',
      surface_water: '8',
      population: '1000',
      residents: [],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/'],
      created: '2014-12-10T11:37:19.144000Z',
      edited: '2014-12-20T20:58:18.421000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/3/'
    },
    {
      name: 'Hooaoac',
      rotation_period: '23',
      orbital_period: '549',
      diameter: '7200',
      climate: 'wwrcooufwowh',
      gravity: '1.1 caorawhwararcwa',
      terrain: 'aohuwhwarcra, ahoawo oarahowoc, scoohuwhaoraahwh rcrawhrrwoc',
      surface_water: '100',
      population: 'huwhorwhooohwh',
      residents: [],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      created: '2014-12-10T11:39:13.934000Z',
      edited: '2014-12-20T20:58:18.423000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/4/'
    },
    {
      name: 'Drarroorhraac',
      rotation_period: '23',
      orbital_period: '341',
      diameter: '8900',
      climate: 'schurcorro',
      gravity: 'N/A',
      terrain: 'cohrascak, shhuwhrranwoc',
      surface_water: '8',
      population: 'huwhorwhooohwh',
      residents: [],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      created: '2014-12-10T11:42:22.590000Z',
      edited: '2014-12-20T20:58:18.425000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/5/'
    },
    {
      name: 'Bwocakahwh',
      rotation_period: '12',
      orbital_period: '5110',
      diameter: '118000',
      climate: 'aowoscakworcraaowo',
      gravity: '1.5 (churcwwraoawo), 1 caorawhwararcwa (Canoohuwa Cahaoro)',
      terrain: 'rrrac rrahrawhao',
      surface_water: '0',
      population: '6000000',
      residents: ['acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/26/'],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      created: '2014-12-10T11:43:55.240000Z',
      edited: '2014-12-20T20:58:18.427000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/6/'
    },
    {
      name: 'Ewhwaoorc',
      rotation_period: '18',
      orbital_period: '402',
      diameter: '4900',
      climate: 'aowoscakworcraaowo',
      gravity: '0.85 caorawhwararcwa',
      terrain: 'wwoorcwocaoc, scoohuwhaoraahwhc, anraorwoc',
      surface_water: '8',
      population: '30000000',
      residents: ['acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/30/'],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'],
      created: '2014-12-10T11:50:29.349000Z',
      edited: '2014-12-20T20:58:18.429000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/7/'
    },
    {
      name: 'Nrarhoooo',
      rotation_period: '26',
      orbital_period: '312',
      diameter: '12120',
      climate: 'aowoscakworcraaowo',
      gravity: '1 caorawhwararcwa',
      terrain: 'rrrcraccro acahananc, cohrascakc, wwoorcwocaoc, scoohuwhaoraahwhc',
      surface_water: '12',
      population: '4500000000',
      residents: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/21/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/35/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/36/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/37/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/38/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/39/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/42/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/60/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/61/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/66/'
      ],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      created: '2014-12-10T11:52:31.066000Z',
      edited: '2014-12-20T20:58:18.430000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/8/'
    },
    {
      name: 'Coorchucoarawhao',
      rotation_period: '24',
      orbital_period: '368',
      diameter: '12240',
      climate: 'aowoscakworcraaowo',
      gravity: '1 caorawhwararcwa',
      terrain: 'oaahaorocoaraakwo, scoohuwhaoraahwhc',
      surface_water: 'huwhorwhooohwh',
      population: '1000000000000',
      residents: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/34/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/55/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/74/'
      ],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/4/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/6/'
      ],
      created: '2014-12-10T11:54:13.921000Z',
      edited: '2014-12-20T20:58:18.432000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/9/'
    },
    {
      name: 'Krascahwhoo',
      rotation_period: '27',
      orbital_period: '463',
      diameter: '19720',
      climate: 'aowoscakworcraaowo',
      gravity: '1 caorawhwararcwa',
      terrain: 'oooaworawh',
      surface_water: '100',
      population: '1000000000',
      residents: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/22/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/72/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/73/'
      ],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/'],
      created: '2014-12-10T12:45:06.577000Z',
      edited: '2014-12-20T20:58:18.434000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/akanrawhwoaoc/10/'
    }
  ]
};
