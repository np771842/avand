export const vehiclesWookieeMock = {
  oaoohuwhao: 39,
  whwokao:
    'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/?akrarrwo=2&wwoorcscraao=ohooooorahwowo',
  akrcwohoahoohuc: null,
  rcwochuanaoc: [
    {
      whrascwo: 'Srawhwa Crcraohanworc',
      scoowawoan: 'Dahrrrrworc Crcraohanworc',
      scrawhhuwwraoaaohurcworc: 'Coorcwoananahra Mahwhahwhrr Coorcakoorcraaoahoowh',
      oaoocao_ahwh_oarcwowaahaoc: '150000',
      anwowhrraoac: '36.8 ',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '30',
      oarcwooh: '46',
      akraccwowhrrworcc: '30',
      oararcrroo_oaraakraoaahaoro: '50000',
      oaoowhchuscrarhanwoc: '2 scoowhaoacc',
      howoacahoaanwo_oaanracc: 'ohacwowoanwowa',
      akahanooaoc: [],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/'
      ],
      oarcworaaowowa: '2014-12-10T15:36:25.724000Z',
      wowaahaowowa: '2014-12-20T21:30:21.661000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/4/'
    },
    {
      whrascwo: 'T-16 corroacooakakworc',
      scoowawoan: 'T-16 corroacooakakworc',
      scrawhhuwwraoaaohurcworc: 'Iwhoaoosc Coorcakoorcraaoahoowh',
      oaoocao_ahwh_oarcwowaahaoc: '14500',
      anwowhrraoac: '10.4 ',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '1200',
      oarcwooh: '1',
      akraccwowhrrworcc: '1',
      oararcrroo_oaraakraoaahaoro: '50',
      oaoowhchuscrarhanwoc: '0',
      howoacahoaanwo_oaanracc: 'rcwoakhuancoorcoarcrawwao',
      akahanooaoc: [],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/'],
      oarcworaaowowa: '2014-12-10T16:01:52.434000Z',
      wowaahaowowa: '2014-12-20T21:30:21.665000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/6/'
    },
    {
      whrascwo: 'X-34 anrawhwacakwowowaworc',
      scoowawoan: 'X-34 anrawhwacakwowowaworc',
      scrawhhuwwraoaaohurcworc: 'SoorcooShuhurh Coorcakoorcraaoahoowh',
      oaoocao_ahwh_oarcwowaahaoc: '10550',
      anwowhrraoac: '3.4 ',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '250',
      oarcwooh: '1',
      akraccwowhrrworcc: '1',
      oararcrroo_oaraakraoaahaoro: '5',
      oaoowhchuscrarhanwoc: 'huwhorwhooohwh',
      howoacahoaanwo_oaanracc: 'rcwoakhuancoorcoarcrawwao',
      akahanooaoc: [],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/'],
      oarcworaaowowa: '2014-12-10T16:13:52.586000Z',
      wowaahaowowa: '2014-12-20T21:30:21.668000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/7/'
    },
    {
      whrascwo: 'TIE/LN caorarcwwahrracaoworc',
      scoowawoan: 'Tohahwh Ioowh Ewhrrahwhwo/Lwh Saorarcwwahrracaoworc',
      scrawhhuwwraoaaohurcworc: 'Sahwowhrarc Fanwowoao Srocaowoscc',
      oaoocao_ahwh_oarcwowaahaoc: 'huwhorwhooohwh',
      anwowhrraoac: '6.4',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '1200',
      oarcwooh: '1',
      akraccwowhrrworcc: '0',
      oararcrroo_oaraakraoaahaoro: '65',
      oaoowhchuscrarhanwoc: '2 wararoc',
      howoacahoaanwo_oaanracc: 'caorarcwwahrracaoworc',
      akahanooaoc: [],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      oarcworaaowowa: '2014-12-10T16:33:52.860000Z',
      wowaahaowowa: '2014-12-20T21:30:21.670000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/8/'
    },
    {
      whrascwo: 'Swhooohcakwowowaworc',
      scoowawoan: 'ao-47 raahrccakwowowaworc',
      scrawhhuwwraoaaohurcworc: 'Iwhoaoosc oaoorcakoorcraaoahoowh',
      oaoocao_ahwh_oarcwowaahaoc: 'huwhorwhooohwh',
      anwowhrraoac: '4.5',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '650',
      oarcwooh: '2',
      akraccwowhrrworcc: '0',
      oararcrroo_oaraakraoaahaoro: '10',
      oaoowhchuscrarhanwoc: 'whoowhwo',
      howoacahoaanwo_oaanracc: 'raahrccakwowowaworc',
      akahanooaoc: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/18/'
      ],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      oarcworaaowowa: '2014-12-15T12:22:12Z',
      wowaahaowowa: '2014-12-20T21:30:21.672000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/14/'
    },
    {
      whrascwo: 'TIE rhooscrhworc',
      scoowawoan: 'TIE/cra rhooscrhworc',
      scrawhhuwwraoaaohurcworc: 'Sahwowhrarc Fanwowoao Srocaowoscc',
      oaoocao_ahwh_oarcwowaahaoc: 'huwhorwhooohwh',
      anwowhrraoac: '7.8',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '850',
      oarcwooh: '1',
      akraccwowhrrworcc: '0',
      oararcrroo_oaraakraoaahaoro: 'whoowhwo',
      oaoowhchuscrarhanwoc: '2 wararoc',
      howoacahoaanwo_oaanracc: 'cakraoawo/akanrawhwoaorarcro rhooscrhworc',
      akahanooaoc: [],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      oarcworaaowowa: '2014-12-15T12:33:15.838000Z',
      wowaahaowowa: '2014-12-20T21:30:21.675000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/16/'
    },
    {
      whrascwo: 'AT-AT',
      scoowawoan: 'Aanan Tworcrcraahwh Arcscoorcwowa Trcrawhcakoorcao',
      scrawhhuwwraoaaohurcworc:
        'Khuraao Drcahhowo Yrarcwac, Iscakworcahraan Dwoakrarcaoscwowhao ooww Mahanahaorarcro Rwocworarcoaac',
      oaoocao_ahwh_oarcwowaahaoc: 'huwhorwhooohwh',
      anwowhrraoac: '20',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '60',
      oarcwooh: '5',
      akraccwowhrrworcc: '40',
      oararcrroo_oaraakraoaahaoro: '1000',
      oaoowhchuscrarhanwoc: 'huwhorwhooohwh',
      howoacahoaanwo_oaanracc: 'raccrahuanao ohraanorworc',
      akahanooaoc: [],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      oarcworaaowowa: '2014-12-15T12:38:25.937000Z',
      wowaahaowowa: '2014-12-20T21:30:21.677000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/18/'
    },
    {
      whrascwo: 'AT-ST',
      scoowawoan: 'Aanan Tworcrcraahwh Soaoohuao Trcrawhcakoorcao',
      scrawhhuwwraoaaohurcworc:
        'Khuraao Drcahhowo Yrarcwac, Iscakworcahraan Dwoakrarcaoscwowhao ooww Mahanahaorarcro Rwocworarcoaac',
      oaoocao_ahwh_oarcwowaahaoc: 'huwhorwhooohwh',
      anwowhrraoac: '2',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '90',
      oarcwooh: '2',
      akraccwowhrrworcc: '0',
      oararcrroo_oaraakraoaahaoro: '200',
      oaoowhchuscrarhanwoc: 'whoowhwo',
      howoacahoaanwo_oaanracc: 'ohraanorworc',
      akahanooaoc: ['acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/13/'],
      wwahanscc: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      oarcworaaowowa: '2014-12-15T12:46:42.384000Z',
      wowaahaowowa: '2014-12-20T21:30:21.679000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/19/'
    },
    {
      whrascwo: 'Saooorcsc IV Tohahwh-Poowa oaanoohuwa oararc',
      scoowawoan: 'Saooorcsc IV Tohahwh-Poowa',
      scrawhhuwwraoaaohurcworc: 'Bwocakahwh Mooaooorcc',
      oaoocao_ahwh_oarcwowaahaoc: '75000',
      anwowhrraoac: '7',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '1500',
      oarcwooh: '2',
      akraccwowhrrworcc: '0',
      oararcrroo_oaraakraoaahaoro: '10',
      oaoowhchuscrarhanwoc: '1 wararo',
      howoacahoaanwo_oaanracc: 'rcwoakhuancoorcoarcrawwao',
      akahanooaoc: [],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      oarcworaaowowa: '2014-12-15T12:58:50.530000Z',
      wowaahaowowa: '2014-12-20T21:30:21.681000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/20/'
    },
    {
      whrascwo: 'Sraahan rhrarcrrwo',
      scoowawoan: 'Moowaahwwahwowa Lhukhurcro Sraahan Brarcrrwo',
      scrawhhuwwraoaaohurcworc:
        'Urhrcahororahrawh Iwhwahucaorcahwoc Chucaooosc Vwoacahoaanwo Dahhoahcahoowh',
      oaoocao_ahwh_oarcwowaahaoc: '285000',
      anwowhrraoac: '30',
      scrak_raaoscoocakacworcahwhrr_cakwowowa: '100',
      oarcwooh: '26',
      akraccwowhrrworcc: '500',
      oararcrroo_oaraakraoaahaoro: '2000000',
      oaoowhchuscrarhanwoc: 'Lahhowo wwoooowa aorawhorc',
      howoacahoaanwo_oaanracc: 'craahan rhrarcrrwo',
      akahanooaoc: [],
      wwahanscc: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'],
      oarcworaaowowa: '2014-12-18T10:44:14.217000Z',
      wowaahaowowa: '2014-12-20T21:30:21.684000Z',
      hurcan: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/24/'
    }
  ]
};
export const vehiclesWookieeTransformedMock = {
  count: 39,
  next: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/?akrarrwo=2&wwoorcscraao=ohooooorahwowo',
  previous: null,
  results: [
    {
      name: 'Srawhwa Crcraohanworc',
      model: 'Dahrrrrworc Crcraohanworc',
      manufacturer: 'Coorcwoananahra Mahwhahwhrr Coorcakoorcraaoahoowh',
      cost_in_credits: '150000',
      length: '36.8 ',
      max_atmosphering_speed: '30',
      crew: '46',
      passengers: '30',
      cargo_capacity: '50000',
      consumables: '2 scoowhaoacc',
      vehicle_class: 'ohacwowoanwowa',
      pilots: [],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/5/'
      ],
      created: '2014-12-10T15:36:25.724000Z',
      edited: '2014-12-20T21:30:21.661000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/4/'
    },
    {
      name: 'T-16 corroacooakakworc',
      model: 'T-16 corroacooakakworc',
      manufacturer: 'Iwhoaoosc Coorcakoorcraaoahoowh',
      cost_in_credits: '14500',
      length: '10.4 ',
      max_atmosphering_speed: '1200',
      crew: '1',
      passengers: '1',
      cargo_capacity: '50',
      consumables: '0',
      vehicle_class: 'rcwoakhuancoorcoarcrawwao',
      pilots: [],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/'],
      created: '2014-12-10T16:01:52.434000Z',
      edited: '2014-12-20T21:30:21.665000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/6/'
    },
    {
      name: 'X-34 anrawhwacakwowowaworc',
      model: 'X-34 anrawhwacakwowowaworc',
      manufacturer: 'SoorcooShuhurh Coorcakoorcraaoahoowh',
      cost_in_credits: '10550',
      length: '3.4 ',
      max_atmosphering_speed: '250',
      crew: '1',
      passengers: '1',
      cargo_capacity: '5',
      consumables: 'huwhorwhooohwh',
      vehicle_class: 'rcwoakhuancoorcoarcrawwao',
      pilots: [],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/'],
      created: '2014-12-10T16:13:52.586000Z',
      edited: '2014-12-20T21:30:21.668000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/7/'
    },
    {
      name: 'TIE/LN caorarcwwahrracaoworc',
      model: 'Tohahwh Ioowh Ewhrrahwhwo/Lwh Saorarcwwahrracaoworc',
      manufacturer: 'Sahwowhrarc Fanwowoao Srocaowoscc',
      cost_in_credits: 'huwhorwhooohwh',
      length: '6.4',
      max_atmosphering_speed: '1200',
      crew: '1',
      passengers: '0',
      cargo_capacity: '65',
      consumables: '2 wararoc',
      vehicle_class: 'caorarcwwahrracaoworc',
      pilots: [],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      created: '2014-12-10T16:33:52.860000Z',
      edited: '2014-12-20T21:30:21.670000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/8/'
    },
    {
      name: 'Swhooohcakwowowaworc',
      model: 'ao-47 raahrccakwowowaworc',
      manufacturer: 'Iwhoaoosc oaoorcakoorcraaoahoowh',
      cost_in_credits: 'huwhorwhooohwh',
      length: '4.5',
      max_atmosphering_speed: '650',
      crew: '2',
      passengers: '0',
      cargo_capacity: '10',
      consumables: 'whoowhwo',
      vehicle_class: 'raahrccakwowowaworc',
      pilots: [
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/1/',
        'acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/18/'
      ],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      created: '2014-12-15T12:22:12Z',
      edited: '2014-12-20T21:30:21.672000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/14/'
    },
    {
      name: 'TIE rhooscrhworc',
      model: 'TIE/cra rhooscrhworc',
      manufacturer: 'Sahwowhrarc Fanwowoao Srocaowoscc',
      cost_in_credits: 'huwhorwhooohwh',
      length: '7.8',
      max_atmosphering_speed: '850',
      crew: '1',
      passengers: '0',
      cargo_capacity: 'whoowhwo',
      consumables: '2 wararoc',
      vehicle_class: 'cakraoawo/akanrawhwoaorarcro rhooscrhworc',
      pilots: [],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      created: '2014-12-15T12:33:15.838000Z',
      edited: '2014-12-20T21:30:21.675000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/16/'
    },
    {
      name: 'AT-AT',
      model: 'Aanan Tworcrcraahwh Arcscoorcwowa Trcrawhcakoorcao',
      manufacturer:
        'Khuraao Drcahhowo Yrarcwac, Iscakworcahraan Dwoakrarcaoscwowhao ooww Mahanahaorarcro Rwocworarcoaac',
      cost_in_credits: 'huwhorwhooohwh',
      length: '20',
      max_atmosphering_speed: '60',
      crew: '5',
      passengers: '40',
      cargo_capacity: '1000',
      consumables: 'huwhorwhooohwh',
      vehicle_class: 'raccrahuanao ohraanorworc',
      pilots: [],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      created: '2014-12-15T12:38:25.937000Z',
      edited: '2014-12-20T21:30:21.677000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/18/'
    },
    {
      name: 'AT-ST',
      model: 'Aanan Tworcrcraahwh Soaoohuao Trcrawhcakoorcao',
      manufacturer:
        'Khuraao Drcahhowo Yrarcwac, Iscakworcahraan Dwoakrarcaoscwowhao ooww Mahanahaorarcro Rwocworarcoaac',
      cost_in_credits: 'huwhorwhooohwh',
      length: '2',
      max_atmosphering_speed: '90',
      crew: '2',
      passengers: '0',
      cargo_capacity: '200',
      consumables: 'whoowhwo',
      vehicle_class: 'ohraanorworc',
      pilots: ['acaoaoakc://cohraakah.wawoho/raakah/akwoooakanwo/13/'],
      films: [
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/',
        'acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'
      ],
      created: '2014-12-15T12:46:42.384000Z',
      edited: '2014-12-20T21:30:21.679000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/19/'
    },
    {
      name: 'Saooorcsc IV Tohahwh-Poowa oaanoohuwa oararc',
      model: 'Saooorcsc IV Tohahwh-Poowa',
      manufacturer: 'Bwocakahwh Mooaooorcc',
      cost_in_credits: '75000',
      length: '7',
      max_atmosphering_speed: '1500',
      crew: '2',
      passengers: '0',
      cargo_capacity: '10',
      consumables: '1 wararo',
      vehicle_class: 'rcwoakhuancoorcoarcrawwao',
      pilots: [],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/2/'],
      created: '2014-12-15T12:58:50.530000Z',
      edited: '2014-12-20T21:30:21.681000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/20/'
    },
    {
      name: 'Sraahan rhrarcrrwo',
      model: 'Moowaahwwahwowa Lhukhurcro Sraahan Brarcrrwo',
      manufacturer: 'Urhrcahororahrawh Iwhwahucaorcahwoc Chucaooosc Vwoacahoaanwo Dahhoahcahoowh',
      cost_in_credits: '285000',
      length: '30',
      max_atmosphering_speed: '100',
      crew: '26',
      passengers: '500',
      cargo_capacity: '2000000',
      consumables: 'Lahhowo wwoooowa aorawhorc',
      vehicle_class: 'craahan rhrarcrrwo',
      pilots: [],
      films: ['acaoaoakc://cohraakah.wawoho/raakah/wwahanscc/3/'],
      created: '2014-12-18T10:44:14.217000Z',
      edited: '2014-12-20T21:30:21.684000Z',
      url: 'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/24/'
    }
  ]
};
