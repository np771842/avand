/* eslint-disable @typescript-eslint/no-explicit-any */
import { wookieeToEn } from '@utils/wookiee/language';

export const transformWooikiee = (obj: any = {}): NonNullable<any> => {
  if (typeof obj !== 'object') {
    return obj;
  }
  if (Array.isArray(obj)) {
    return obj.map(transformWooikiee);
  }

  return Object.entries(obj).reduce((prv, [key, val]) => {
    if (Array.isArray(val)) {
      return { ...prv, [wookieeToEn(key)]: val.map(transformWooikiee) };
    }

    if (typeof val === 'object' && val !== null) {
      return { ...prv, [wookieeToEn(key)]: transformWooikiee(val) };
    }

    return { ...prv, [wookieeToEn(key)]: val };
  }, {});
};
