import {
  planetWookieeMock,
  planetWookieeTransformedMock,
  speciesWookieeMock,
  speciesWookieeTransformedMock,
  vehiclesWookieeMock,
  vehiclesWookieeTransformedMock
} from '@mocks';
import { transformWooikiee } from './wookieTransformer';

describe('transformWooikiee ', () => {
  describe('format planet', () => {
    test('should transform wookiee deep nested planet object', () => {
      const transformed = transformWooikiee(planetWookieeMock);
      expect(transformed).toEqual(planetWookieeTransformedMock);
    });

    test('should transform wookiee deep nested planet object passed as array', () => {
      const transformed = transformWooikiee([planetWookieeMock]);
      expect(transformed).toEqual([planetWookieeTransformedMock]);
    });
  });

  describe('format species', () => {
    test('should transform wookiee deep nested species object', () => {
      const transformed = transformWooikiee(speciesWookieeMock);
      expect(transformed).toEqual(speciesWookieeTransformedMock);
    });
    test('should transform wookiee deep nested species object passed as nested array', () => {
      const transformed = transformWooikiee([[speciesWookieeMock]]);
      expect(transformed).toEqual([[speciesWookieeTransformedMock]]);
    });
  });

  describe('format vehicles', () => {
    test('should transform wookiee deep nested vehicles object', () => {
      const transformed = transformWooikiee(vehiclesWookieeMock);
      expect(transformed).toEqual(vehiclesWookieeTransformedMock);
    });
    test('should transform wookiee deep nested vehicles object passed as nested array', () => {
      const transformed = transformWooikiee([[[vehiclesWookieeMock]]]);
      expect(transformed).toEqual([[[vehiclesWookieeTransformedMock]]]);
    });
  });
});
