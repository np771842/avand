const LOOKUP: Record<string, string> = {
  a: 'ra',
  b: 'rh',
  c: 'oa',
  d: 'wa',
  e: 'wo',
  f: 'ww',
  g: 'rr',
  h: 'ac',
  i: 'ah',
  j: 'sh',
  k: 'or',
  l: 'an',
  m: 'sc',
  n: 'wh',
  o: 'oo',
  p: 'ak',
  q: 'rq',
  r: 'rc',
  s: 'c',
  t: 'ao',
  u: 'hu',
  v: 'ho',
  w: 'oh',
  x: 'k',
  y: 'ro',
  z: 'uf'
};

const wookieeMap = Object.fromEntries(Object.entries(LOOKUP).map(item => item.reverse()));

export const wookieeToEn = (str = ''): string => {
  let english = '';
  let offset = 0;
  const chars = str.toLocaleLowerCase();

  while (offset < chars.length) {
    const cur = chars.charAt(offset);
    const nxt = chars.slice(offset, offset + 2);
    const currlookup = wookieeMap[cur];
    const nxtLookup = wookieeMap[nxt];

    if (currlookup) {
      english += currlookup;
    } else if (nxtLookup) {
      english += nxtLookup;
      offset += 1;
    } else {
      english += cur;
    }
    offset += 1;
  }

  return english;
};

export const enToWookiee = (str = ''): string =>
  [...str.toLocaleLowerCase()].map((key: string) => (LOOKUP[key] ? LOOKUP[key] : key)).join('');
