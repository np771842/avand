import { wookieeToEn, enToWookiee } from './language';

describe('wookieeToEn', () => {
  test('translate normal wookiee to english', () => {
    const wookiee = 'oaoohuwhao';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('count');
  });

  test('translate special wookiee containing k & c as wookiee text', () => {
    const wookiee = 'whwokao';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('next');
  });

  test('translate special wookiee containing k & c and special char for ex _', () => {
    const wookiee = 'oaoohuwhao_whwokao';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('count_next');
  });

  test('translate wookiee containing special char for ex _', () => {
    const wookiee = 'rcooaoraaoahoowh_akworcahoowa';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('rotation_period');
  });

  test('translate wookiee containing k & c and multiple special char', () => {
    const wookiee = 'whwokao_^$%@#9NH_hurcan';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('next_^$%@#9nh_url');
  });

  test('translate wookiee containing multiple special char', () => {
    const wookiee = 'scoowawoan_^$%@#9nh_oaoohuwhao';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('model_^$%@#9nh_count');
  });

  test('should translate wookiee url to normal htttp url', () => {
    const wookiee =
      'acaoaoakc://cohraakah.wawoho/raakah/howoacahoaanwoc/?akrarrwo=2&wwoorcscraao=ohooooorahwowo';
    const english = wookieeToEn(wookiee);
    expect(english).toEqual('https://swapi.dev/api/vehicles/?page=2&format=wookiee');
  });
});

describe('enToWookiee', () => {
  test('translate english to wookiee', () => {
    const wookiee = 'count';
    const english = enToWookiee(wookiee);
    expect(english).toEqual('oaoohuwhao');
  });

  test('translate english containing k & c as english text', () => {
    const english = 'next';
    const wookiee = enToWookiee(english);
    expect(wookiee).toEqual('whwokao');
  });

  test('translate english for ex _', () => {
    const english = 'count_next';
    const wookiee = enToWookiee(english);
    expect(wookiee).toEqual('oaoohuwhao_whwokao');
  });

  test('translate english containing multiple special char', () => {
    const english = 'next_^$%@#9_url';
    const wookiee = enToWookiee(english);
    expect(wookiee).toEqual('whwokao_^$%@#9_hurcan');
  });
});
