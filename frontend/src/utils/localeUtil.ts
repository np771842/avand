import { LOCALE } from '@utils/constants';
import i18n from '@utils/i18';

export const wookiee = {
  get isWookiee(): boolean {
    return i18n.language === LOCALE.WOOKIEE;
  }
};
