import { HTTP_ROUTES, IMAGE_HOST } from '@utils/constants';

export const getResourceId = (url: string): string => url.split('/')[url.split('/').length - 2];

export const getResourceImageUrl = (resourceRoute: string, url: string): string =>
  `${IMAGE_HOST}/${resourceRoute}${getResourceId(url)}.jpg`;

export const getCategoryImageUrl = (category: string): string =>
  `${IMAGE_HOST}/categories/${category}.jpg`;

export const getPlanetImageurl = (url: string): string =>
  getResourceImageUrl(HTTP_ROUTES.PLANETS, url);

export const getSpeciesImageurl = (url: string): string =>
  getResourceImageUrl(HTTP_ROUTES.SPECIES, url);

export const getVehiclesImageurl = (url: string): string =>
  getResourceImageUrl(HTTP_ROUTES.VEHICLES, url);

export const getStarShipsImageurl = (url: string): string =>
  getResourceImageUrl(HTTP_ROUTES.STARSHIPS, url);

export const getCategoryImageurl = (category: string): string => getCategoryImageUrl(category);
