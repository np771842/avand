export const isJsonString = (str: string): boolean => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

export const parse = (str?: string | null): Record<string, unknown> =>
  str && isJsonString(str) ? JSON.parse(str) : {};

export const stringify = (obj?: unknown): string => (obj ? JSON.stringify(obj) : '');
