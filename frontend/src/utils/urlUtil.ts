import i18n from '@utils/i18';
import { LOCALE, FORMAT, HOST } from '@utils/constants';
import { wookieeToEn } from '@utils/wookiee/language';
import { wookiee } from '@utils/localeUtil';

export const getUrlPath = (route: string): string => `${HOST}${route}`;

export const getUrl = (url: string): string => {
  const enrichedUrl = new URL(url);
  if (wookiee.isWookiee) {
    enrichedUrl.searchParams.set(FORMAT, LOCALE.WOOKIEE);
  }

  return enrichedUrl.href;
};

export const getTranslatedUrl = (url: string): string =>
  i18n.language === LOCALE.WOOKIEE ? wookieeToEn(url) : url;
