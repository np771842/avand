import { getVehicleType } from '@utils/data';
import { IStartshipApiData, IStartshipData, IVehicleApiData, IVehicleData } from '@types';

export const transformVehicleData = (vehiclesData: IVehicleApiData[]): IVehicleData[] => {
  const transformedVehicleData = vehiclesData.map((vehicle: IVehicleApiData) => {
    const {
      name,
      model,
      max_atmosphering_speed: speed,
      vehicle_class: vehicleClass,
      url
    } = vehicle;
    return {
      model,
      name,
      speed: Number(speed),
      type: getVehicleType(Number(speed)),
      url,
      vehicleClass
    };
  });
  return transformedVehicleData;
};

export const transformStarShipsData = (starShipData: IStartshipApiData[]): IStartshipData[] => {
  const transformedStarShipData = starShipData.map((starShip: IStartshipApiData) => {
    const {
      hyperdrive_rating: hyperDriveRating,
      max_atmosphering_speed: speed,
      model,
      name,
      starship_class: starshipClass,
      url
    } = starShip;
    return {
      hyperDriveRating,
      model,
      name,
      speed: Number(speed),
      starshipClass,
      url
    };
  });
  return transformedStarShipData;
};
