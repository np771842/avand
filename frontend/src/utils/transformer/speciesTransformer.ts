import { getSpeciesResourceCategory } from '@utils/data/species';
import { ISpeciesApiData, ISpeciesData, ISpeciesTransformedData } from '@types';

export const getSpeciesData = (speciesData: ISpeciesApiData[]): ISpeciesTransformedData[] => {
  const transformedSpeciesData = speciesData.map((species: ISpeciesApiData) => {
    const {
      classification,
      designation,
      homeworld,
      language,
      name,
      skin_colors: skinColors,
      url
    } = species;
    return {
      classification: classification?.split(','),
      designation: designation?.split(','),
      homeworld,
      language,
      name,
      skinColors: skinColors?.split(','),
      url
    };
  });
  return transformedSpeciesData;
};

export const transformSpeciesData = (
  speciesData: ISpeciesApiData[],
  checkAlliance = false,
  planetId?: string
): ISpeciesData[] => {
  const speciesTransformedData = getSpeciesData(speciesData);
  const transformedSpeciesData = speciesTransformedData.map((species: ISpeciesTransformedData) => {
    const { name, classification, designation, skinColors, homeworld, language, url } = species;
    return {
      classification,
      designation,
      homeworld,
      language,
      name,
      skinColors,
      speciesCategory: getSpeciesResourceCategory(species, checkAlliance, planetId),
      url
    };
  });
  return transformedSpeciesData;
};
