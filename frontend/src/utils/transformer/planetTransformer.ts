import { getLandMass, getPlanetResourceCategory, sortByParams } from '@utils/data';
import { IPlanetApiData, IPlanetData, IPlanetTransformedData } from '@types';

export const getPlanetData = (planetData: IPlanetApiData[]): IPlanetTransformedData[] => {
  const transformedPlanetData = planetData.map((planet: IPlanetApiData) => {
    const { population, terrain, climate, gravity, name, url } = planet;
    const landMass = getLandMass(planet);
    return {
      climate: climate?.split(','),
      gravity: Number(gravity?.split(' ')[0]),
      landMass,
      landMassPopulationRatio: landMass / Number(population),
      name,
      population: Number(population),
      terrain: terrain?.split(','),
      url
    };
  });
  return transformedPlanetData;
};

export const transformPlanetData = (planetData: IPlanetApiData[]): IPlanetData[] => {
  const planetTransformedData = sortByParams(getPlanetData(planetData));
  const transformedPlanetData = planetTransformedData.map((planet: IPlanetTransformedData) => {
    const { population, terrain, climate, gravity, name, url, landMass, landMassPopulationRatio } =
      planet;
    return {
      climate,
      gravity,
      landMass,
      landMassPopulationRatio,
      name,
      planetCategory: getPlanetResourceCategory(planet),
      population,
      terrain,
      url
    };
  });
  return transformedPlanetData;
};
