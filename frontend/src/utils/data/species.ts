import { getResourceId } from '@utils/imgUrl';
import { ISpeciesTransformedData, RESOURCE_CATEGORY } from '@types';
import { classification as spClassification } from '@utils/constants/species';

export const isAlliancePossible = (species: ISpeciesTransformedData, planetId?: string): boolean =>
  species.homeworld ? getResourceId(species.homeworld) !== planetId : true;

const isFavouriteClassification = (classifications: string[]): boolean =>
  classifications.some(cl => spClassification.favouriteClassification.indexOf(cl.trim()) >= 0);

const isFavouriteDesignation = (designations: string[]): boolean =>
  designations.some(ds => spClassification.favouriteDesignation.indexOf(ds.trim()) >= 0);

const isUndesirableClassification = (classifications: string[]): boolean =>
  classifications.some(cl => spClassification.undesirableClassification.indexOf(cl.trim()) >= 0);

const isUndesirableSkinColor = (skinColores: string[]): boolean =>
  skinColores.some(colors => spClassification.undesirableSkinColors.indexOf(colors.trim()) >= 0);

const isFavouriteSpecies = ({
  classification,
  designation,
  skinColors
}: ISpeciesTransformedData): boolean =>
  isFavouriteClassification(classification) &&
  isFavouriteDesignation(designation) &&
  !isUndesirableSkinColor(skinColors);

const isUndesirableSpecies = ({ classification, skinColors }: ISpeciesTransformedData): boolean =>
  isUndesirableClassification(classification) || isUndesirableSkinColor(skinColors);

export const getSpeciesResourceCategory = (
  species: ISpeciesTransformedData,
  alliance = false,
  planetId?: string
): RESOURCE_CATEGORY => {
  const isAlliance = !alliance || isAlliancePossible(species, planetId);
  const isFavourite = isFavouriteSpecies(species) && isAlliance;
  const isUndesirable = isUndesirableSpecies(species);
  const isConsiderable = !isFavourite && !isUndesirable;

  switch (true) {
    case isFavourite:
      return RESOURCE_CATEGORY.FAVOURITE;
    case isUndesirable:
      return RESOURCE_CATEGORY.REJECTED;
    case isConsiderable:
    default:
      return RESOURCE_CATEGORY.CONSIDERABLE;
  }
};
