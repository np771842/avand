import { EXCLUDED_STARSHIP, LAND_VEHCICLE_LIMIT, VEHICLE_TYPE } from '@utils/constants/vehicle';
import { IStartshipData, IVehicleData } from '@types';

const isAerialVehicle = (speed: number): boolean =>
  !Number.isNaN(speed) && speed > LAND_VEHCICLE_LIMIT;

export const fetchArialVehicles = (vehicleResults: IVehicleData[]): IVehicleData[] =>
  vehicleResults.filter(({ speed }) => isAerialVehicle(speed));

export const fetchLandVehicles = (vehicleResults: IVehicleData[]): IVehicleData[] =>
  vehicleResults.filter(({ speed }) => !isAerialVehicle(speed));

export const fetchStarShips = (starShipResults: IStartshipData[]): IStartshipData[] =>
  starShipResults.filter(({ name }) => EXCLUDED_STARSHIP.indexOf(name) === -1);

export const getVehicleType = (speed: number): VEHICLE_TYPE =>
  isAerialVehicle(speed) ? VEHICLE_TYPE.AERIAL : VEHICLE_TYPE.LAND;
