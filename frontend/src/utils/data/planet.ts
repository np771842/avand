/* eslint-disable no-restricted-globals */
import { ACCEPTABLE_GRAVITY_RANGE, filterCriteria } from '@utils/constants/planet';
import { IPlanetApiData, IPlanetTransformedData, RESOURCE_CATEGORY } from '@types';

const isValidGravity = (gravity: number): boolean =>
  gravity >= ACCEPTABLE_GRAVITY_RANGE.LOWER && gravity <= ACCEPTABLE_GRAVITY_RANGE.UPPER;

const isFavouriteClimate = (climates: string[]): boolean =>
  climates.some(cl => filterCriteria.favouriteClimate.indexOf(cl.trim()) >= 0);

const isFavouriteTerrain = (terrains: string[]): boolean =>
  terrains.some(tr => filterCriteria.favouriteTerrain.indexOf(tr.trim()) >= 0);

const isFavouriteGravity = (gravity: number): boolean =>
  !Number.isNaN(gravity) && isValidGravity(gravity);

const isUndesirableClimate = (climates: string[]): boolean =>
  climates.some(cl => filterCriteria.undesirableClimate.indexOf(cl.trim()) >= 0);

const isUndesirableGravity = (gravity: number): boolean =>
  !Number.isNaN(gravity) && !isValidGravity(gravity);

const compareLandMass = (a: IPlanetTransformedData, b: IPlanetTransformedData): number =>
  b.landMass - a.landMass || +isNaN(a.landMass) - +isNaN(b.landMass);

const compareLandMassPopulationRatio = (
  a: IPlanetTransformedData,
  b: IPlanetTransformedData
): number =>
  b.landMass / b.population - a.landMass / a.population || +isNaN(a.landMass) - +isNaN(b.landMass);

export const sortByParams = (planetData: IPlanetTransformedData[]): IPlanetTransformedData[] =>
  planetData.sort(compareLandMass).sort(compareLandMassPopulationRatio);

export const getLandMass = ({ diameter, surface_water: surfaceWater }: IPlanetApiData): number => {
  const surfaceArea = Number(diameter) ** 2 * Math.PI;
  return ((100 - Number(surfaceWater)) / 100) * surfaceArea;
};

export const getPlanetResourceCategory = (planet: IPlanetTransformedData): RESOURCE_CATEGORY => {
  const { climate, gravity, terrain } = planet;
  const isFavourite =
    isFavouriteClimate(climate) && isFavouriteGravity(gravity) && isFavouriteTerrain(terrain);
  const isUndesirable = isUndesirableClimate(climate) || isUndesirableGravity(gravity);
  const isConsiderable = !isFavourite && !isUndesirable;

  switch (true) {
    case isFavourite:
      return RESOURCE_CATEGORY.FAVOURITE;
    case isUndesirable:
      return RESOURCE_CATEGORY.REJECTED;
    case isConsiderable:
    default:
      return RESOURCE_CATEGORY.CONSIDERABLE;
  }
};
