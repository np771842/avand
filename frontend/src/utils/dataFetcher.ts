import { IRequest } from '@types';
import Api from '@utils/api';
import { transformWooikiee } from '@utils/wookiee/wookieTransformer';
import { getUrl } from '@utils/urlUtil';
import { wookiee } from '@utils/localeUtil';

const fetchUrl = async <T>({ body, headers, method, url }: IRequest): Promise<T> => {
  const { data } = await Api.execute<T>({
    body,
    headers,
    method,
    url: getUrl(url)
  });

  return wookiee.isWookiee ? transformWooikiee(data) : data;
};

export { fetchUrl };
