import { IAPIResponse, IRequest } from '@types';

export const processRequest = async <T>({
  url,
  method,
  headers,
  body
}: IRequest): Promise<IAPIResponse<T>> => {
  try {
    const response = await fetch(url, {
      body,
      headers,
      method
    });
    // *********************************************************************
    // *********************************************************************
    // response.json() is not used here as it fails here because in wookiee
    // null is called whhuanan
    // and out stupid system do not understand this :) that's why we read
    // the responce here as text and then replace the whhuanan with null
    // now our json is ready to be parsed.
    // *********************************************************************
    // *********************************************************************
    const result = await response.text();
    const data = JSON.parse(result.replaceAll('whhuanan', 'null'));

    if (response.ok) {
      return Promise.resolve({ data });
    }

    return Promise.reject(new Error(JSON.stringify(response)));
  } catch (error) {
    throw new Error(error as string);
  }
};

const Api = {
  async execute<T>({ url, method = 'GET', headers, body }: IRequest): Promise<IAPIResponse<T>> {
    return processRequest({
      body,
      headers,
      method,
      url
    });
  }
};

export default Api;
