import DOMPurify from 'dompurify';

export const sanitize = (dirty: string): string => DOMPurify.sanitize(dirty);
