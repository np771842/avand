export const NAMESPACE = 'translation_i18n';

export enum ROUTES {
  ALLIANCE = '/alliance',
  HOME = '/',
  PLANETS = '/planets',
  SPECIES = '/species',
  VEHICLES = '/vehicles'
}

export const HTTP_ROUTES = {
  PLANETS: 'planets/',
  SPECIES: 'species/',
  STARSHIPS: 'starships/',
  VEHICLES: 'vehicles/'
};

export const RESOURCE = {
  PLANETS: 'planets',
  SPECIES: 'species',
  STARSHIPS: 'starships',
  VEHICLES: 'vehicles'
};

export const ORIGIN = 'https://swapi.dev';
export const PATHNAME = '/api/';
export const HOST = `${ORIGIN}${PATHNAME}`;
export const IMAGE_HOST = 'https://starwars-visualguide.com/assets/img';
export const LOGO_IMG_URL =
  'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Star_Wars_Logo.svg/1388px-Star_Wars_Logo.svg.png';

export const LOCALE = {
  EN: 'en',
  WOOKIEE: 'wookiee'
};

export const FORMAT = 'format';

export const PAGE_OFFSET = 10;
