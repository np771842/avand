import { wookiee } from '@utils/localeUtil';
import { enToWookiee } from '@utils/wookiee/language';

const FAVOURITE_CLASSIFICATION = ['mammal', 'gastropod', 'amphibian', 'human'];
const FAVOURITE_DESIGNATION = ['sentient'];
const UNDESIRABLE_CLASSIFICATION = ['droids,', 'reptilians'];
const UNDESIRABLE_SKIN_COLORS = ['pink', 'peach'];

export const classification = {
  get favouriteClassification(): string[] {
    return wookiee.isWookiee ? FAVOURITE_CLASSIFICATION.map(enToWookiee) : FAVOURITE_CLASSIFICATION;
  },
  get favouriteDesignation(): string[] {
    return wookiee.isWookiee ? FAVOURITE_DESIGNATION.map(enToWookiee) : FAVOURITE_DESIGNATION;
  },
  get undesirableClassification(): string[] {
    return wookiee.isWookiee
      ? UNDESIRABLE_CLASSIFICATION.map(enToWookiee)
      : UNDESIRABLE_CLASSIFICATION;
  },
  get undesirableSkinColors(): string[] {
    return wookiee.isWookiee ? UNDESIRABLE_SKIN_COLORS.map(enToWookiee) : UNDESIRABLE_SKIN_COLORS;
  }
};
