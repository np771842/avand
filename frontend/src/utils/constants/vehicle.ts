export const LAND_VEHCICLE_LIMIT = 100;
export const EXCLUDED_STARSHIP = ['Death Star'];

export enum VEHICLE_TYPE {
  AERIAL = 'Aerial Vehicle',
  LAND = 'Land Vehicle'
}
