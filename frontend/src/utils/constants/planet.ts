import { wookiee } from '@utils/localeUtil';
import { enToWookiee } from '@utils/wookiee/language';

export const FAVOURITE_CLIMATE = ['temperate', 'arid', 'tropical'];
export const FAVOURITE_TERRAIN = ['mountains', 'forests', 'valleys'];
export const UNDESIRABLE_CLIMATE = ['arctic,', 'subarctic,', 'frigid', 'frozen'];

export enum ACCEPTABLE_GRAVITY_RANGE {
  LOWER = 0.85,
  UPPER = 1.2
}

export const filterCriteria = {
  get favouriteClimate(): string[] {
    return wookiee.isWookiee ? FAVOURITE_CLIMATE.map(enToWookiee) : FAVOURITE_CLIMATE;
  },
  get favouriteTerrain(): string[] {
    return wookiee.isWookiee ? FAVOURITE_TERRAIN.map(enToWookiee) : FAVOURITE_TERRAIN;
  },
  get undesirableClimate(): string[] {
    return wookiee.isWookiee ? UNDESIRABLE_CLIMATE.map(enToWookiee) : UNDESIRABLE_CLIMATE;
  }
};
