export enum RESOURCE_CATEGORY {
  FAVOURITE = 'favourite',
  CONSIDERABLE = 'considerable',
  REJECTED = 'rejected'
}
