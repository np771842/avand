import { RESOURCE_CATEGORY } from '../common';

/* eslint-disable camelcase */
export interface IPlanetApiData {
  diameter: string;
  population: string;
  surface_water: string;
  terrain: string;
  climate: string;
  gravity: string;
  name: string;
  url: string;
}

export interface IPlanetTransformedData {
  name: string;
  gravity: number;
  climate: string[];
  terrain: string[];
  population: number;
  landMass: number;
  url: string;
  landMassPopulationRatio: number;
}

export interface IPlanetData extends IPlanetTransformedData {
  planetCategory: RESOURCE_CATEGORY;
}

export interface IPlanetCategory {
  favourite: IPlanetData[];
  considerable: IPlanetData[];
  rejected: IPlanetData[];
}
