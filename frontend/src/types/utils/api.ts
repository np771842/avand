export interface IResponse<T> {
  results: T[];
  count: number;
}

export interface IAPIResponse<T> {
  error?: string;
  status?: string;
  data?: T;
}

export interface IRequest {
  body?: string;
  headers?: HeadersInit;
  method?: string;
  url: string;
  wookieActive?: boolean;
}

export interface IFetch<T> {
  error: unknown;
  loading: boolean;
  data?: T;
}
