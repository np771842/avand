/* eslint-disable camelcase */
import { VEHICLE_TYPE } from '@utils/constants/vehicle';

export interface IVehicleResponse {
  results: IVehicleApiData[];
  next?: string;
}

export interface IStarshipResponse {
  results: IStartshipApiData[];
  next?: string;
}

export interface IVehicleApiData {
  model: string;
  max_atmosphering_speed: string;
  vehicle_class: string;
  name: string;
  url: string;
}

export interface IVehicleData {
  name: string;
  model: string;
  speed: number;
  vehicleClass: string;
  url: string;
  type: VEHICLE_TYPE;
}

export interface IStartshipApiData {
  hyperdrive_rating: string;
  max_atmosphering_speed: string;
  model: string;
  name: string;
  starship_class: string;
  url: string;
}

export interface IStartshipData {
  name: string;
  model: string;
  speed: number;
  hyperDriveRating: string;
  starshipClass: string;
  url: string;
}
