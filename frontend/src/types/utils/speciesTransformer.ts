import { RESOURCE_CATEGORY } from '../common';

/* eslint-disable camelcase */
export interface ISpeciesResponse {
  results: ISpeciesApiData[];
  next?: string;
}

export interface ISpeciesApiData {
  classification: string;
  designation: string;
  skin_colors: string;
  homeworld: string;
  language: string;
  name: string;
  url: string;
}

export interface ISpeciesTransformedData {
  name: string;
  classification: string[];
  designation: string[];
  skinColors: string[];
  homeworld: string;
  language: string;
  url: string;
}

export interface ISpeciesData extends ISpeciesTransformedData {
  speciesCategory: RESOURCE_CATEGORY;
}

export interface ISpeciesCategory {
  favourite: ISpeciesData[];
  considerable: ISpeciesData[];
  rejected: ISpeciesData[];
}
