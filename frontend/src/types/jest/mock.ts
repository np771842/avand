export interface I18NextTMock {
  t: (key: string) => string;
}

export interface I18NextMock {
  useTranslation: () => I18NextTMock;
}
