import { ReactElement } from 'react';

export interface ISmokeCleanerProps {
  children: ReactElement;
}
