import { ISpeciesData } from '../utils/speciesTransformer';

export interface ISpeciesListItemProp {
  species: ISpeciesData[];
}

export interface ISpeciesDetailsProp {
  species: ISpeciesData;
}

export interface ISpeciesListProp extends ISpeciesListItemProp {
  fetchMore: () => void;
}

export interface IPlanetIdParam {
  id: string;
}
