import { IStartshipData, IVehicleData } from '../utils/vehicleTransformer';

export interface IVehicleListItemProp {
  vehicle: IVehicleData[];
}

export interface IVehicleDetailsProp {
  vehicle: IVehicleData;
}

export interface IVehicleListProp extends IVehicleListItemProp {
  fetchMore: () => void;
}

export interface IStarshipListItemProp {
  starships: IStartshipData[];
}

export interface IStarShipDetailsProp {
  starship: IStartshipData;
}
