export interface IListItemProps {
  name: string;
  url: string;
  children: React.ReactNode;
}
