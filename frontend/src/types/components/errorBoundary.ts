import { ReactNode } from 'react';

export interface IErrorBoundaryProps {
  children: ReactNode;
  t: (arg: string) => string;
}

export interface IErrorBoundaryState {
  hasError: boolean;
}
