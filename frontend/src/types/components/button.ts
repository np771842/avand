export interface IButtonProps {
  children?: React.ReactNode;
  className: string;
  onClick: () => void;
  label: string;
}

export interface INavigationButtonProps {
  onPreviousClick: () => void;
  onNextClick: () => void;
}
