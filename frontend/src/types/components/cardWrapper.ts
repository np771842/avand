export interface ICardProps {
  backgroundImage: string;
  route: string;
  title: string;
}
