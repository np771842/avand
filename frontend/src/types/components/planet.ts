import { IPlanetData } from '../utils/planetTransformer';

export interface IPlanetListItemProp {
  planets: IPlanetData[];
}

export interface IPlanetDetailsProp {
  planet: IPlanetData;
}
