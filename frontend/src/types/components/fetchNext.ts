export interface IFetchNext {
  start: number;
  end: number;
}
