export interface ICategoryItemProps {
  categoryName: string;
  categoryContent: React.ReactNode;
}

export interface ICategoryProps {
  categoryList: ICategoryItemProps[];
}

export interface ICategoryHeaderProps extends ICategoryProps {
  activeIndex: number;
  onClick: (index: number) => void;
}
