import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import '@utils/i18';
import App from '@components/App';
import { registerServiceWorker } from './sw/serviceWorkerRegistration';

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
);
registerServiceWorker();
