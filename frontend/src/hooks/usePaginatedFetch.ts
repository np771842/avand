import { fetchUrl } from '@utils/dataFetcher';
import { useEffect, useState } from 'react';
import { IRequest, IFetch, IResponse } from '@types';
import { useTranslation } from 'react-i18next';
import { NAMESPACE, PAGE_OFFSET } from '@utils/constants';

export const usePaginatedFetch = <T>({ url }: IRequest): IFetch<T[]> => {
  const { i18n } = useTranslation(NAMESPACE, { useSuspense: false });
  const [data, setData] = useState<T[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<unknown>(undefined);
  const [count, setCount] = useState<number>(0);

  const fetchFirstPage = async (): Promise<void> => {
    const response = await fetchUrl<IResponse<T>>({ url });
    const { results, count: num } = response;
    setData([...data, ...results]);
    setCount(num);
  };

  const fetchPagesOtherThenFirst = async (): Promise<void> => {
    const pages: number = Math.ceil(count / PAGE_OFFSET);
    const urls = Array.from({ length: pages }, (_, i) => `${url}?page=${i + 1}`).slice(1);

    const response = await Promise.all(urls.map(uri => fetchUrl<IResponse<T>>({ url: uri }))).then(
      res =>
        res.reduce(
          (prev, curr) => ({
            ...prev,
            ...curr,
            results: ([] as T[]).concat(prev?.results ?? [], curr?.results ?? [])
          }),
          { results: [] as T[] }
        )
    );

    const { results } = response;
    setData([...data, ...results]);
    setLoading(false);
  };

  useEffect(() => {
    try {
      if (count) {
        fetchPagesOtherThenFirst();
      } else {
        fetchFirstPage();
      }
    } catch (err) {
      setError(error);
    }
  }, [count]);

  useEffect(() => {
    setData([]);
    setLoading(true);
    setError(undefined);
    setCount(0);
  }, [i18n.language]);

  return { data, error, loading };
};
