import CardWrapper from '@components/CardWrapper';
import React, { FC, ReactElement } from 'react';

const Home: FC = (): ReactElement => <CardWrapper />;

export default Home;
