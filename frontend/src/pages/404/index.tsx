import React, { FC, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { NAMESPACE } from '@utils/constants';
import './index.scss';

const NotFound: FC = (): ReactElement => {
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });

  return (
    <div id="notfound">
      <div className="notfound-bg" />
      <div className="notfound">
        <div className="notfound-404">
          <h1>{t('404')}</h1>
        </div>
        <h2>{t('404_description')}</h2>
        <Link className="home-btn" to="/">
          {t('go_home')}
        </Link>
      </div>
    </div>
  );
};

export default NotFound;
