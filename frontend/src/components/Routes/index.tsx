import React, { FC, ReactElement } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ROUTES } from '@utils/constants';
import Home from 'frontend/src/pages/Home';
import NotFound from 'frontend/src/pages/404';
import PlanetList from '@components/PlanetList';
import SpiciesList from '@components/SpeciesList';
import VehicleList from '@components/VehicleList';
import Alliance from '@components/AllianceList';

const Routes: FC = (): ReactElement => (
  <Router>
    <Switch>
      <Route exact component={Home} path={ROUTES.HOME} />
      <Route exact component={PlanetList} path={ROUTES.PLANETS} />
      <Route exact component={SpiciesList} path={ROUTES.SPECIES} />
      <Route exact component={VehicleList} path={ROUTES.VEHICLES} />
      <Route component={Alliance} path={`${ROUTES.ALLIANCE}/:id`} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);

export default Routes;
