import { IVehicleDetailsProp } from '@types';
import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { NAMESPACE } from '@utils/constants';

const VehicleDetails: FC<IVehicleDetailsProp> = ({ vehicle }): ReactElement => {
  const { name, model, speed, vehicleClass } = vehicle;
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });
  return (
    <>
      <h3>{name}</h3>
      <p>{`${t('model')}: ${model}`}</p>
      <p>{`${t('speed')}: ${speed}`}</p>
      <p>{`${t('vehicleClass')}: ${vehicleClass}`}</p>
    </>
  );
};

export default VehicleDetails;
