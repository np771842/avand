import React, { FC, ReactElement, useEffect, useState } from 'react';
import { ISpeciesDetailsProp, IPlanetApiData } from '@types';
import { fetchUrl } from '@utils/dataFetcher';
import { useTranslation } from 'react-i18next';
import { NAMESPACE } from '@utils/constants';
import { getTranslatedUrl } from '@utils/urlUtil';

const SpeciesDetails: FC<ISpeciesDetailsProp> = ({ species }): ReactElement => {
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });
  const [data, setData] = useState<IPlanetApiData>();
  const { name, classification, designation, language, homeworld: url, skinColors } = species;

  useEffect(() => {
    const fetchHomePlanet = async (): Promise<void> => {
      const res = await fetchUrl<IPlanetApiData>({ url: getTranslatedUrl(url) });
      setData(res);
    };
    fetchHomePlanet();
  }, [url]);

  return (
    <>
      <h3>{name}</h3>
      <p>{`${t('classification')}: ${classification}`}</p>
      <p>{`${t('designation')}: ${designation}`}</p>
      <p>{`${t('language')}: ${language}`}</p>
      {data?.name && <p>{`${t('homePlanet')}: ${data?.name}`}</p>}
      <p>{`${t('skinColors')}: ${skinColors}`}</p>
    </>
  );
};

export default SpeciesDetails;
