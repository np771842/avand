import { FC, useEffect, ReactElement } from 'react';
import { ISmokeCleanerProps } from '@types';

// TODO: Find a better way to smoke test and remove this completally
const SmokeCleaner: FC<ISmokeCleanerProps> = ({ children }): ReactElement => {
  useEffect(() => {
    document.getElementById('version')?.remove();
  }, []);

  return children;
};

export default SmokeCleaner;
