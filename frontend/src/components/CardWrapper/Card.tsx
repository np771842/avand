import React, { FC, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { ICardProps } from '@types';

const Card: FC<ICardProps> = ({ backgroundImage, route, title }): ReactElement => (
  <Link
    className="category-link"
    style={{ backgroundImage: `url(${backgroundImage})` }}
    to={{
      pathname: `${route}`
    }}
  >
    <div className="desc">
      <h3 className="category-title">{title}</h3>
    </div>
  </Link>
);

export default Card;
