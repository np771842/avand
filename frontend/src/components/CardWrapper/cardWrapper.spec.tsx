/* eslint-disable prettier/prettier */
import { render, waitFor } from '@testing-library/react';

import CardWrapper from '@components/CardWrapper';
import { withRouter } from '@test';

describe('Card Wrapper', () => {
  it('should show the planets species and vehicle cards', () => {
    const { getByText } = render(withRouter(CardWrapper, '/'));
    waitFor(() => expect(getByText('PLANETS')).toBeInTheDocument());
    waitFor(() => expect(getByText('SPECIES')).toBeInTheDocument());
    waitFor(() => expect(getByText('VEHICLES')).toBeInTheDocument());
  });
});
