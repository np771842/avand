import React, { FC, ReactElement } from 'react';
import { NAMESPACE, RESOURCE, ROUTES } from '@utils/constants';
import { getCategoryImageurl } from '@utils/imgUrl';
import Card from '@components/CardWrapper/Card';
import { useTranslation } from 'react-i18next';

const CardWrapper: FC = (): ReactElement => {
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });
  return (
    <div className="vertical-list">
      <div className="grid grid--columns category grid--gap">
        <Card
          backgroundImage={getCategoryImageurl(RESOURCE.PLANETS)}
          route={ROUTES.PLANETS}
          title={`${t('planets')}`}
        />
        <Card
          backgroundImage={getCategoryImageurl(RESOURCE.SPECIES)}
          route={ROUTES.SPECIES}
          title={`${t('species')}`}
        />
        <Card
          backgroundImage={getCategoryImageurl(RESOURCE.VEHICLES)}
          route={ROUTES.VEHICLES}
          title={`${t('vehicle')}`}
        />
      </div>
    </div>
  );
};

export default CardWrapper;
