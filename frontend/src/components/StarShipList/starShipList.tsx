import ListItem from '@components/shared/ListItem';
import StarShipDetails from '@components/StarShipDetails';
import { IStarshipListItemProp, IStartshipData } from '@types';
import { getStarShipsImageurl } from '@utils/imgUrl';
import React, { FC, ReactElement } from 'react';

const StarShipList: FC<IStarshipListItemProp> = ({ starships }): ReactElement => (
  <>
    <ul className="grid grid--columns grid--gap">
      {starships.map((starShip: IStartshipData) => (
        <ListItem
          key={starShip.name}
          name={starShip.name}
          url={`${getStarShipsImageurl(starShip.url)}`}
        >
          <StarShipDetails starship={starShip} />
        </ListItem>
      ))}
    </ul>
  </>
);

export default StarShipList;
