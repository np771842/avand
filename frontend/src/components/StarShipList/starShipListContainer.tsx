import { useTranslation } from 'react-i18next';
import ErrorMessage from '@components/shared/Error';
import { NavigationButtons } from '@components/shared/NavigationButton';
import Spinner from '@components/shared/Spinner';
import { IFetchNext, IStarshipResponse, IStartshipData } from '@types';
import { getTranslatedUrl, getUrl, getUrlPath } from '@utils/urlUtil';
import { HTTP_ROUTES, NAMESPACE, PAGE_OFFSET } from '@utils/constants';
import { fetchUrl } from '@utils/dataFetcher';
import { transformStarShipsData } from '@utils/transformer/vehicleTransformer';
import React, { FC, ReactElement, useEffect, useState } from 'react';
import StarShipList from '@components/StarShipList/starShipList';
import { fetchStarShips } from '@utils/data';

const StarShipListContainer: FC = (): ReactElement => {
  const { i18n } = useTranslation(NAMESPACE, { useSuspense: false });
  const starShipRoute = getUrl(getUrlPath(HTTP_ROUTES.STARSHIPS));
  const [startShips, setStarships] = useState<IStartshipData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>('');
  const [next, setNext] = useState<string>('');
  const [route, setRoute] = useState<string>(starShipRoute);
  const [pageCount, setPageCount] = useState<number>(1);

  useEffect(() => {
    setStarships([]);
    setError('');
    setNext('');
    setRoute(starShipRoute);
  }, [i18n.language]);

  const getPageIndex = (page: number): IFetchNext => ({
    end: startShips.length >= page * PAGE_OFFSET ? page * PAGE_OFFSET : startShips.length,
    start: page > 0 ? page : 1
  });

  const getData = (): IStartshipData[] => {
    const { start, end } = getPageIndex(pageCount);
    return startShips.slice(start * PAGE_OFFSET - PAGE_OFFSET, end);
  };

  useEffect(() => {
    const fetchPaginatedResponse = async (): Promise<void> => {
      const response = await fetchUrl<IStarshipResponse>({ url: route });
      if (response) {
        const { results = [], next: more = '' } = response;
        const filteredStarShips = fetchStarShips(transformStarShipsData(results));
        setStarships([...startShips, ...filteredStarShips]);
        setLoading(false);
        setNext(more);
      }
    };

    const { start } = getPageIndex(pageCount);
    if (startShips.length > start * PAGE_OFFSET) {
      return;
    }
    fetchPaginatedResponse().catch(eror => setError(eror));
  }, [route]);

  const onNextClick = (): void => {
    if (next) {
      setRoute(getTranslatedUrl(next));
      setPageCount(pageCount + 1);
    } else if (startShips.length > pageCount * PAGE_OFFSET) {
      setPageCount(pageCount + 1);
    }
  };

  const onPreviousClick = (): void => {
    setPageCount(getPageIndex(pageCount - 1).start);
  };

  return (
    <>
      {loading && <Spinner />}
      {error ? (
        <ErrorMessage />
      ) : (
        <>
          <div className="vertical-list">
            <NavigationButtons onNextClick={onNextClick} onPreviousClick={onPreviousClick} />
            <StarShipList starships={getData()} />
          </div>
        </>
      )}
    </>
  );
};

export default StarShipListContainer;
