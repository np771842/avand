import React, { FC, ReactElement } from 'react';
import StarShipListContainer from './starShipListContainer';

const StarShips: FC = (): ReactElement => <StarShipListContainer />;

export default StarShips;
