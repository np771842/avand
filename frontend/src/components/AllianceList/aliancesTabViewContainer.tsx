import ErrorMessage from '@components/shared/Error';
import Spinner from '@components/shared/Spinner';
import CategoryTabs from '@components/shared/Tabs';
import SpeciesListContainer from '@components/SpeciesList/speciesListContainer';
import { HTTP_ROUTES, NAMESPACE } from '@utils/constants';
import { fetchUrl } from '@utils/dataFetcher';
import { transformSpeciesData } from '@utils/transformer/speciesTransformer';
import { ISpeciesData, ISpeciesResponse, ICategoryItemProps, IPlanetIdParam } from '@types';
import React, { FC, ReactElement, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { getTranslatedUrl, getUrlPath } from '@utils/urlUtil';

const AlliancesTabViewContainer: FC = (): ReactElement => {
  const { i18n, t } = useTranslation(NAMESPACE, { useSuspense: false });
  const speciesRoute = getUrlPath(HTTP_ROUTES.SPECIES);
  const [species, setSpecies] = useState<ISpeciesData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>('');
  const [next, setNext] = useState<string>('');
  const [route, setRoute] = useState<string>(speciesRoute);
  const { id: planetId } = useParams<IPlanetIdParam>();

  useEffect(() => {
    setSpecies([]);
    setError('');
    setNext('');
    setRoute(speciesRoute);
  }, [i18n.language]);

  useEffect(() => {
    const fetchPaginatedResponse = async (): Promise<void> => {
      const response = await fetchUrl<ISpeciesResponse>({ url: route });
      if (response) {
        const { results = [], next: more = '' } = response;
        const transformedSpeciesData = transformSpeciesData(results, true, planetId);
        setSpecies([...species, ...transformedSpeciesData]);
        setLoading(false);
        setNext(more);
      }
    };
    fetchPaginatedResponse().catch(eror => setError(eror));
  }, [route, planetId]);

  const fetchMore = (): void => {
    if (next) {
      setRoute(getTranslatedUrl(next));
    }
  };

  const getFavouriteData = (): ISpeciesData[] =>
    species.filter(item => item.speciesCategory === 'favourite');
  const getConsiderableData = (): ISpeciesData[] =>
    species.filter(item => item.speciesCategory === 'considerable');

  const categoryList = (): ICategoryItemProps[] => [
    {
      categoryContent: <SpeciesListContainer fetchMore={fetchMore} species={getFavouriteData()} />,
      categoryName: t('favouriteAlliances')
    },
    {
      categoryContent: (
        <SpeciesListContainer fetchMore={fetchMore} species={getConsiderableData()} />
      ),
      categoryName: t('considerableAlliances')
    }
  ];

  return (
    <>
      {loading && <Spinner />}
      {error ? <ErrorMessage /> : <CategoryTabs categoryList={categoryList()} />}
    </>
  );
};

export default AlliancesTabViewContainer;
