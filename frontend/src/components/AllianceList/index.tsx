import React, { FC, ReactElement } from 'react';
import AlliancesTabViewContainer from '@components/AllianceList/aliancesTabViewContainer';

const Alliance: FC = (): ReactElement => <AlliancesTabViewContainer />;

export default Alliance;
