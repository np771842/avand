import { IStarShipDetailsProp } from '@types';
import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { NAMESPACE } from '@utils/constants';

const StarShipDetails: FC<IStarShipDetailsProp> = ({ starship }): ReactElement => {
  const { name, model, hyperDriveRating, starshipClass, speed } = starship;
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });

  return (
    <>
      <h3>{name}</h3>
      <p>{`${t('model')}: ${model}`}</p>
      <p>{`${t('starshipClass')}: ${starshipClass}`}</p>
      <p>{`${t('speed')}: ${speed}`}</p>
      <p>{`${t('hyperdriveRating')}: ${hyperDriveRating}`}</p>
    </>
  );
};

export default StarShipDetails;
