import React, { FC, ReactElement } from 'react';

const Spinner: FC = (): ReactElement => (
  <div className="spinner" data-testid="spinner">
    <div className="lds-ring">
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

export default Spinner;
