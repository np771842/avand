import React, { FC } from 'react';
import { IButtonProps } from '@types';

export const Button: FC<IButtonProps> = ({ children, label, className, onClick }) => (
  <button className={`${className} button-generic`} onClick={onClick} type="button">
    {label}
    {children}
  </button>
);
