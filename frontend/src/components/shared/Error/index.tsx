import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { IErrorMessageProps } from '@types';
import { NAMESPACE } from '@utils/constants';
import './index.scss';

const ErrorMessage: FC<IErrorMessageProps> = ({ message }): ReactElement => {
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });
  const errorMessage = message ?? t('error_message');

  return (
    <div className="error" data-testid="error-message">
      <h3>{errorMessage}</h3>
    </div>
  );
};

export default ErrorMessage;
