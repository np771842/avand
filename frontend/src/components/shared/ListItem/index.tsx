import { IListItemProps } from '@types';
import React, { FC, ReactElement } from 'react';

const ListItem: FC<IListItemProps> = ({ name, url, children }): ReactElement => (
  <li key={name} style={{ backgroundImage: `url(${url})` }}>
    <div className="desc">{children}</div>
  </li>
);

export default ListItem;
