import { INavigationButtonProps } from '@types';
import React, { FC } from 'react';
import { Button } from '@components/shared/Button';
import { useTranslation } from 'react-i18next';
import { NAMESPACE } from '@utils/constants';

export const NavigationButtons: FC<INavigationButtonProps> = ({ onPreviousClick, onNextClick }) => {
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });
  return (
    <div className="nav">
      <Button className="secondary" label={`<< ${t('previous')}`} onClick={onPreviousClick} />
      <Button className="secondary" label={`${t('next')} >>`} onClick={onNextClick} />
    </div>
  );
};
