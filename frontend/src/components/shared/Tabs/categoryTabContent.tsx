import { ICategoryHeaderProps } from '@types';
import React, { FC } from 'react';

const CategoryTabContent: FC<ICategoryHeaderProps> = ({ categoryList, activeIndex, onClick }) => (
  <div className="content-tabs">
    {categoryList.map(({ categoryContent }, index) => (
      <div
        className={activeIndex === index + 1 ? 'content active-content' : 'content'}
        key={index.toString()}
        onClick={(): void => onClick(index + 1)}
        role="tab"
        tabIndex={0}
      >
        {categoryContent}
      </div>
    ))}
  </div>
);

export default CategoryTabContent;
