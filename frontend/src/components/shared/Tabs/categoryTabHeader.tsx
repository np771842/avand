import { ICategoryHeaderProps } from '@types';
import React, { FC } from 'react';

const CategoryTabHeader: FC<ICategoryHeaderProps> = ({ categoryList, activeIndex, onClick }) => (
  <div className="bloc-tabs">
    {categoryList.map((category, index) => (
      <div
        className={activeIndex === index + 1 ? 'tabs active-tabs' : 'tabs'}
        key={index.toString()}
        onClick={(): void => onClick(index + 1)}
        role="tab"
        tabIndex={0}
      >
        <h2>{category.categoryName}</h2>
      </div>
    ))}
  </div>
);

export default CategoryTabHeader;
