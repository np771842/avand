import { ICategoryProps } from '@types';
import React, { FC, useState } from 'react';
import CategoryTabContent from '@components/shared/Tabs/categoryTabContent';
import CategoryTabHeader from '@components/shared/Tabs/categoryTabHeader';

const CategoryTabs: FC<ICategoryProps> = ({ categoryList }) => {
  const [toggleState, setToggleState] = useState<number>(1);
  const toggleTab = (index: number): void => setToggleState(index);

  return (
    <div className="tab-container">
      <CategoryTabHeader
        activeIndex={toggleState}
        categoryList={categoryList}
        onClick={toggleTab}
      />
      <CategoryTabContent
        activeIndex={toggleState}
        categoryList={categoryList}
        onClick={toggleTab}
      />
    </div>
  );
};

export default CategoryTabs;
