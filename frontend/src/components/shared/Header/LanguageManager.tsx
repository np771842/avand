import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { LOCALE, NAMESPACE } from '@utils/constants';

export const LanguageManager: FC = (): ReactElement => {
  const { i18n } = useTranslation(NAMESPACE, { useSuspense: false });

  const changeLanguageHandler = (): void => {
    i18n.changeLanguage(i18n.language === LOCALE.EN ? LOCALE.WOOKIEE : LOCALE.EN);
  };

  return (
    <button className="primary inline locale" onClick={changeLanguageHandler} type="button">
      {i18n.language.toLocaleUpperCase()}
    </button>
  );
};
