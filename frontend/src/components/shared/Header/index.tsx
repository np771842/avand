import React, { FC, ReactElement } from 'react';
import { LanguageManager } from '@components/shared/Header/LanguageManager';
import { LOGO_IMG_URL } from '@utils/constants';

const Header: FC = (): ReactElement => (
  <div className="header">
    <div className="header-content">
      <a href="/">
        <img alt="Star Wars" className="logo-img" src={LOGO_IMG_URL} />
      </a>
      <LanguageManager />
    </div>
  </div>
);

export default Header;
