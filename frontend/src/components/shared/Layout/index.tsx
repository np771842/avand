import React, { FC, ReactElement } from 'react';
import Header from '@components/shared/Header';

const Layout: FC = ({ children }): ReactElement => (
  <div className="container">
    <Header />
    {children}
  </div>
);

export default Layout;
