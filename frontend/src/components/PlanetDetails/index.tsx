import { NAMESPACE, ROUTES } from '@utils/constants';
import { getResourceId } from '@utils/imgUrl';
import { IPlanetDetailsProp } from '@types';
import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const PlanetDetails: FC<IPlanetDetailsProp> = ({ planet }): ReactElement => {
  const { name, climate, terrain, population, gravity, url, landMass, landMassPopulationRatio } =
    planet;
  const { t } = useTranslation(NAMESPACE, { useSuspense: false });
  return (
    <>
      <h3>{name}</h3>
      <p>{`${t('climate')}: ${climate}`}</p>
      <p>{`${t('terrain')}: ${terrain}`}</p>
      <p>{`${t('population')}: ${population}`}</p>
      <p>{`${t('gravity')}: ${gravity}`}</p>
      <p>{`${t('landMass')}: ${landMass}`}</p>
      <p>{`${t('populationDensity')}: ${landMassPopulationRatio}`}</p>
      <Link
        className="resource-link"
        to={{
          pathname: `${ROUTES.ALLIANCE}/${getResourceId(url)}`
        }}
      >
        {t('alliance')} &raquo;
      </Link>
      <Link
        className="resource-link"
        to={{
          pathname: `${ROUTES.VEHICLES}`
        }}
      >
        {t('vehicle')} &raquo;
      </Link>
    </>
  );
};

export default PlanetDetails;
