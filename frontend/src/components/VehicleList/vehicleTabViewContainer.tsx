import ErrorMessage from '@components/shared/Error';
import Spinner from '@components/shared/Spinner';
import CategoryTabs from '@components/shared/Tabs';
import StarShipListContainer from '@components/StarShipList/starShipListContainer';
import { getUrlPath, getTranslatedUrl } from '@utils/urlUtil';
import { HTTP_ROUTES, NAMESPACE } from '@utils/constants';
import { VEHICLE_TYPE } from '@utils/constants/vehicle';
import { fetchUrl } from '@utils/dataFetcher';
import { useTranslation } from 'react-i18next';
import { transformVehicleData } from '@utils/transformer/vehicleTransformer';
import { IVehicleData, IVehicleResponse, ICategoryItemProps } from '@types';
import React, { FC, ReactElement, useEffect, useState } from 'react';
import LandAerialListContainer from '@components/VehicleList/vehicleListContainer';

const VehiclesTabViewContainer: FC = (): ReactElement => {
  const { i18n, t } = useTranslation(NAMESPACE, { useSuspense: false });
  const vehicleRoute = getUrlPath(HTTP_ROUTES.VEHICLES);
  const [vehicles, setVehicles] = useState<IVehicleData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>('');
  const [next, setNext] = useState<string>('');
  const [route, setRoute] = useState<string>(vehicleRoute);

  useEffect(() => {
    setVehicles([]);
    setError('');
    setNext('');
    setRoute(vehicleRoute);
  }, [i18n.language]);

  useEffect(() => {
    const fetchPaginatedResponse = async (): Promise<void> => {
      const response = await fetchUrl<IVehicleResponse>({ url: route });
      if (response) {
        const { results = [], next: more = '' } = response;
        const transformedVehicleData = transformVehicleData(results);
        setVehicles([...vehicles, ...transformedVehicleData]);
        setLoading(false);
        setNext(more);
      }
    };
    fetchPaginatedResponse().catch(eror => setError(eror));
  }, [route]);

  const fetchMore = (): void => {
    if (next) {
      setRoute(getTranslatedUrl(next));
    }
  };

  const getAerialVehicle = (): IVehicleData[] =>
    vehicles.filter(item => item.type === VEHICLE_TYPE.AERIAL);
  const getLandVehicle = (): IVehicleData[] =>
    vehicles.filter(item => item.type === VEHICLE_TYPE.LAND);

  const categoryList = (): ICategoryItemProps[] => [
    {
      categoryContent: (
        <LandAerialListContainer fetchMore={fetchMore} vehicle={getAerialVehicle()} />
      ),
      categoryName: t('aerialVehicle')
    },
    {
      categoryContent: <LandAerialListContainer fetchMore={fetchMore} vehicle={getLandVehicle()} />,
      categoryName: t('landVehicle')
    },
    {
      categoryContent: <StarShipListContainer />,
      categoryName: t('starShip')
    }
  ];

  return (
    <>
      {loading && <Spinner />}
      {error ? <ErrorMessage /> : <CategoryTabs categoryList={categoryList()} />}
    </>
  );
};

export default VehiclesTabViewContainer;
