import React, { FC, ReactElement } from 'react';
import VehiclesTabViewContainer from '@components/VehicleList/vehicleTabViewContainer';

const Vehicles: FC = (): ReactElement => <VehiclesTabViewContainer />;

export default Vehicles;
