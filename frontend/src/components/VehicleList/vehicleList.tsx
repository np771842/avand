import ListItem from '@components/shared/ListItem';
import VehicleDetails from '@components/VehicleDetails';
import { IVehicleListItemProp, IVehicleData } from '@types';
import { getVehiclesImageurl } from '@utils/imgUrl';
import React, { FC, ReactElement } from 'react';

const VehicleList: FC<IVehicleListItemProp> = ({ vehicle }): ReactElement => (
  <>
    <ul className="grid grid--columns grid--gap">
      {vehicle.map((veh: IVehicleData) => (
        <ListItem key={veh.url} name={veh.name} url={`${getVehiclesImageurl(veh.url)}`}>
          <VehicleDetails vehicle={veh} />
        </ListItem>
      ))}
    </ul>
  </>
);

export default VehicleList;
