import React, { FC, ReactElement, useEffect, useState } from 'react';
import { NavigationButtons } from '@components/shared/NavigationButton';
import { IFetchNext, IVehicleListProp, IVehicleData } from '@types';
import { PAGE_OFFSET } from '@utils/constants';
import VehicleList from '@components/VehicleList/vehicleList';

const LandAerialListContainer: FC<IVehicleListProp> = ({ vehicle, fetchMore }): ReactElement => {
  const [pageCount, setPageCount] = useState<number>(1);

  const getPageIndex = (page: number): IFetchNext => ({
    end: vehicle.length >= page * PAGE_OFFSET ? page * PAGE_OFFSET : vehicle.length,
    start: vehicle.length >= page * PAGE_OFFSET ? page : Math.ceil(vehicle.length / PAGE_OFFSET)
  });

  const getData = (): IVehicleData[] => {
    const { start, end } = getPageIndex(pageCount);

    return vehicle.slice(start * PAGE_OFFSET - PAGE_OFFSET, end);
  };

  const onNextButtonClick = (): void => {
    if (vehicle.length > pageCount * PAGE_OFFSET) {
      setPageCount(pageCount + 1);
    } else {
      fetchMore();
    }
  };

  const onPreviousClick = (): void => {
    if (pageCount > 1) {
      setPageCount(pageCount - 1);
    }
  };

  useEffect(() => {
    if (pageCount > 0 && vehicle.length < pageCount * PAGE_OFFSET) {
      fetchMore();
    }
  }, [pageCount, vehicle]);

  return (
    <div className="vertical-list">
      <NavigationButtons onNextClick={onNextButtonClick} onPreviousClick={onPreviousClick} />
      <div className="vertical-list">
        <VehicleList vehicle={getData()} />
      </div>
    </div>
  );
};

export default LandAerialListContainer;
