import { NavigationButtons } from '@components/shared/NavigationButton';
import { IFetchNext, ISpeciesListProp, ISpeciesData } from '@types';
import { PAGE_OFFSET } from '@utils/constants';
import React, { FC, ReactElement, useEffect, useState } from 'react';
import SpeciesList from '@components/SpeciesList/speciesList';

const SpeciesListContainer: FC<ISpeciesListProp> = ({ species, fetchMore }): ReactElement => {
  const [recordCount, setRecordCount] = useState<number>(1);

  const getPageIndex = (page: number): IFetchNext => ({
    end: species.length >= page * PAGE_OFFSET ? page * PAGE_OFFSET : species.length,
    start: species.length >= page * PAGE_OFFSET ? page : Math.ceil(species.length / PAGE_OFFSET)
  });

  const getData = (): ISpeciesData[] => {
    const { start, end } = getPageIndex(recordCount);

    return species.slice(start * PAGE_OFFSET - PAGE_OFFSET, end);
  };

  const onNextButtonClick = (): void => {
    if (species.length > recordCount * PAGE_OFFSET) {
      setRecordCount(recordCount + 1);
    } else {
      fetchMore();
    }
  };

  const onPreviousClick = (): void => {
    if (recordCount > 1) {
      setRecordCount(recordCount - 1);
    }
  };

  useEffect(() => {
    if (recordCount > 0 && species.length < recordCount * PAGE_OFFSET) {
      fetchMore();
    }
  }, [recordCount, species]);

  return (
    <div className="vertical-list">
      <NavigationButtons onNextClick={onNextButtonClick} onPreviousClick={onPreviousClick} />
      <SpeciesList species={getData()} />
    </div>
  );
};

export default SpeciesListContainer;
