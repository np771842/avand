import React, { FC, ReactElement, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ICategoryItemProps, ISpeciesData, ISpeciesResponse } from '@types';
import ErrorMessage from '@components/shared/Error';
import Spinner from '@components/shared/Spinner';
import CategoryTabs from '@components/shared/Tabs';
import { getUrlPath, getTranslatedUrl } from '@utils/urlUtil';
import { HTTP_ROUTES, NAMESPACE } from '@utils/constants';
import { fetchUrl } from '@utils/dataFetcher';
import { transformSpeciesData } from '@utils/transformer/speciesTransformer';
import SpeciesListContainer from './speciesListContainer';

const SpeciesTabViewContainer: FC = (): ReactElement => {
  const { i18n, t } = useTranslation(NAMESPACE, { useSuspense: false });
  const speciesRoute = getUrlPath(HTTP_ROUTES.SPECIES);
  const [species, setSpecies] = useState<ISpeciesData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string>('');
  const [next, setNext] = useState<string>('');
  const [route, setRoute] = useState<string>(speciesRoute);

  useEffect(() => {
    setSpecies([]);
    setError('');
    setNext('');
    setRoute(speciesRoute);
  }, [i18n.language]);

  useEffect(() => {
    const fetchPaginatedResponse = async (): Promise<void> => {
      const response = await fetchUrl<ISpeciesResponse>({ url: route });
      if (response) {
        const { results = [], next: more = '' } = response;
        const transformedSpeciesData = transformSpeciesData(results);

        setSpecies([...species, ...transformedSpeciesData]);
        setLoading(false);
        setNext(more);
      }
    };
    fetchPaginatedResponse().catch(eror => setError(eror));
  }, [route]);

  const fetchMore = (): void => {
    if (next) {
      setRoute(getTranslatedUrl(next));
    }
  };

  const getFavouriteData = (): ISpeciesData[] =>
    species.filter(item => item.speciesCategory === 'favourite');
  const getConsiderableData = (): ISpeciesData[] =>
    species.filter(item => item.speciesCategory === 'considerable');
  const getRejectedData = (): ISpeciesData[] =>
    species.filter(item => item.speciesCategory === 'rejected');

  const categoryList = (): ICategoryItemProps[] => [
    {
      categoryContent: <SpeciesListContainer fetchMore={fetchMore} species={getFavouriteData()} />,
      categoryName: t('favouriteAlliances')
    },
    {
      categoryContent: (
        <SpeciesListContainer fetchMore={fetchMore} species={getConsiderableData()} />
      ),
      categoryName: t('considerableAlliances')
    },
    {
      categoryContent: <SpeciesListContainer fetchMore={fetchMore} species={getRejectedData()} />,
      categoryName: t('rejectedAlliances')
    }
  ];

  return (
    <>
      {loading && <Spinner />}
      {error ? <ErrorMessage /> : <CategoryTabs categoryList={categoryList()} />}
    </>
  );
};

export default SpeciesTabViewContainer;
