import ListItem from '@components/shared/ListItem';
import SpeciesDetails from '@components/SpeciesDetails';
import { getSpeciesImageurl } from '@utils/imgUrl';
import { ISpeciesListItemProp, ISpeciesData } from '@types';
import React, { FC, ReactElement } from 'react';

const SpeciesList: FC<ISpeciesListItemProp> = ({ species }): ReactElement => (
  <>
    {species.length > 0 && (
      <>
        <ul className="grid grid--columns grid--gap">
          {species.map((item: ISpeciesData) => (
            <ListItem key={item.name} name={item.name} url={getSpeciesImageurl(item.url)}>
              <SpeciesDetails species={item} />
            </ListItem>
          ))}
        </ul>
      </>
    )}
  </>
);

export default SpeciesList;
