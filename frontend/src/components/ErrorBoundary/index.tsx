import React, { Component, ErrorInfo, ReactNode } from 'react';
import { withTranslation } from 'react-i18next';
import { IErrorBoundaryProps, IErrorBoundaryState } from '@types';
import { NAMESPACE } from '@utils/constants';
import ErrorMessage from '@components/shared/Error';

class ErrorBoundary extends Component<IErrorBoundaryProps, IErrorBoundaryState> {
  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(): IErrorBoundaryState {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    // eslint-disable-next-line no-console
    console.error('Uncaught error from error boundry:', error, errorInfo);
  }

  render(): ReactNode {
    const { hasError } = this.state;
    const { t, children } = this.props;

    if (hasError) {
      return <ErrorMessage message={t('error_boundry')} />;
    }

    return children;
  }
}

export default withTranslation(NAMESPACE)(ErrorBoundary);
