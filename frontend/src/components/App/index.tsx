import React, { FC, ReactElement, Suspense } from 'react';
import Spinner from '@components/shared/Spinner';
import SmokeCleaner from '@components/SmokeCleaner';
import ErrorBoundary from '@components/ErrorBoundary';
import Layout from '@components/shared/Layout';
import Routes from '@components/Routes';
import '@assets/Scss/index.scss';

const App: FC = (): ReactElement => (
  <Suspense fallback={<Spinner />}>
    <SmokeCleaner>
      <ErrorBoundary>
        <Layout>
          <div className="app-main">
            <Routes />
          </div>
        </Layout>
      </ErrorBoundary>
    </SmokeCleaner>
  </Suspense>
);

export default App;
