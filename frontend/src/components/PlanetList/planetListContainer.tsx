import { NavigationButtons } from '@components/shared/NavigationButton';
import { IFetchNext, IPlanetListItemProp, IPlanetData } from '@types';
import { PAGE_OFFSET } from '@utils/constants';
import React, { FC, ReactElement, useState } from 'react';
import PlanetList from '@components/PlanetList/planetList';

const PlanetListContainer: FC<IPlanetListItemProp> = ({ planets }): ReactElement => {
  const [recordCount, setRecordCount] = useState<number>(1);

  const getPageIndex = (page: number): IFetchNext => ({
    end: planets.length >= page * PAGE_OFFSET ? page * PAGE_OFFSET : planets.length,
    start: page > 0 ? page : 1
  });

  const getData = (): IPlanetData[] => {
    const { start, end } = getPageIndex(recordCount);
    return planets.slice(start * PAGE_OFFSET - PAGE_OFFSET, end);
  };

  const onNextButtonClick = (): void => {
    if (planets.length > recordCount * PAGE_OFFSET) {
      setRecordCount(recordCount + 1);
    }
  };

  const onPreviousClick = (): void => {
    setRecordCount(getPageIndex(recordCount - 1).start);
  };

  return (
    <div className="vertical-list">
      <NavigationButtons onNextClick={onNextButtonClick} onPreviousClick={onPreviousClick} />
      <PlanetList planets={getData()} />
    </div>
  );
};

export default PlanetListContainer;
