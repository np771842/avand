import ErrorMessage from '@components/shared/Error';
import Spinner from '@components/shared/Spinner';
import CategoryTabs from '@components/shared/Tabs';
import { getUrlPath } from '@utils/urlUtil';
import { HTTP_ROUTES, NAMESPACE } from '@utils/constants';
import { transformPlanetData } from '@utils/transformer/planetTransformer';
import { usePaginatedFetch } from 'frontend/src/hooks/usePaginatedFetch';
import { IPlanetApiData, IPlanetData, ICategoryItemProps, RESOURCE_CATEGORY } from '@types';
import React, { FC, ReactElement, useEffect, useState } from 'react';
import PlanetListContainer from '@components/PlanetList/planetListContainer';
import { useTranslation } from 'react-i18next';

const PlanetsTabViewContainer: FC = (): ReactElement => {
  const planetRoute = getUrlPath(HTTP_ROUTES.PLANETS);
  const [planets, setPlanets] = useState<IPlanetData[]>([]);
  const [route, setRoute] = useState<string>(planetRoute);

  const { data, loading, error } = usePaginatedFetch<IPlanetApiData>({
    url: route
  });
  const { i18n, t } = useTranslation(NAMESPACE, { useSuspense: false });

  useEffect(() => {
    setPlanets([]);
    setRoute(planetRoute);
  }, [i18n.language]);

  useEffect(() => {
    if (data) {
      const transformedPlanetData = transformPlanetData(data);
      setPlanets(transformedPlanetData);
    }
  }, [data]);

  const getFavouriteData = (): IPlanetData[] =>
    planets.filter(item => item.planetCategory === RESOURCE_CATEGORY.FAVOURITE);
  const getConsiderableData = (): IPlanetData[] =>
    planets.filter(item => item.planetCategory === RESOURCE_CATEGORY.CONSIDERABLE);
  const getRejectedData = (): IPlanetData[] =>
    planets.filter(item => item.planetCategory === RESOURCE_CATEGORY.REJECTED);

  const categoryList = (): ICategoryItemProps[] => [
    {
      categoryContent: <PlanetListContainer planets={getFavouriteData()} />,
      categoryName: t('favouritePlanets')
    },
    {
      categoryContent: <PlanetListContainer planets={getConsiderableData()} />,
      categoryName: t('considerablePlanets')
    },
    {
      categoryContent: <PlanetListContainer planets={getRejectedData()} />,
      categoryName: t('rejectedPlanets')
    }
  ];

  if (loading) {
    return <Spinner />;
  }

  return <>{error ? <ErrorMessage /> : <CategoryTabs categoryList={categoryList()} />}</>;
};

export default PlanetsTabViewContainer;
