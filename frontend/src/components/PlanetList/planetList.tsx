import PlanetDetails from '@components/PlanetDetails';
import ListItem from '@components/shared/ListItem';
import { getPlanetImageurl } from '@utils/imgUrl';
import { IPlanetListItemProp, IPlanetData } from '@types';
import React, { FC, ReactElement } from 'react';

const PlanetList: FC<IPlanetListItemProp> = ({ planets }): ReactElement => (
  <>
    <ul className="grid grid--columns grid--gap">
      {planets.map((planet: IPlanetData) => (
        <ListItem key={planet.url} name={planet.name} url={getPlanetImageurl(planet.url)}>
          <PlanetDetails planet={planet} />
        </ListItem>
      ))}
    </ul>
  </>
);

export default PlanetList;
