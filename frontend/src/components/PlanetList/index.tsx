import React, { FC, ReactElement } from 'react';
import PlanetsTabViewContainer from '@components/PlanetList/planetTabViewContainer';

const PlanetList: FC = (): ReactElement => <PlanetsTabViewContainer />;

export default PlanetList;
