/* eslint-disable no-restricted-globals */
/* eslint-disable no-underscore-dangle */
import { clientsClaim } from 'workbox-core';
import { ExpirationPlugin } from 'workbox-expiration';
import { registerRoute } from 'workbox-routing';
import { StaleWhileRevalidate, CacheFirst } from 'workbox-strategies';
import { CacheableResponsePlugin } from 'workbox-cacheable-response';
import { precacheAndRoute } from 'workbox-precaching';

const ORIGIN = 'https://swapi.dev';
const PATHNAME = '/api/';

clientsClaim();
self.skipWaiting();
precacheAndRoute(self.__WB_MANIFEST);

registerRoute(
  ({ url }) => url.origin === ORIGIN && url.pathname.startsWith(PATHNAME),

  new StaleWhileRevalidate({
    cacheName: 'star-wars-api',
    plugins: [
      new CacheableResponsePlugin({
        statuses: [0, 200]
      }),
      new ExpirationPlugin({
        maxAgeSeconds: 1 * 24 * 60 * 60
      })
    ]
  })
);

registerRoute(
  ({ request }) => request.destination === 'image',
  new CacheFirst({
    cacheName: 'star-wars-images',
    plugins: [
      new CacheableResponsePlugin({
        statuses: [0, 200]
      }),
      new ExpirationPlugin({
        maxAgeSeconds: 1 * 24 * 60 * 60
      })
    ]
  })
);
