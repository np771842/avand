import { Workbox } from 'workbox-window';

export const registerServiceWorker = (): void => {
  if ('serviceWorker' in navigator) {
    const wb = new Workbox('sw.js');
    wb.register();
  }
};
