/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/jsx-props-no-spreading */
import React, { ReactElement } from 'react';
import { Route, Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';

const history = createMemoryHistory();

export const withRouter = (
  Component: React.FC,
  path: string,
  mocks?: any,
  props?: any
): ReactElement => (
  <Router history={history}>
    <Route path={path}>
      <Component {...props} />
    </Route>
  </Router>
);
