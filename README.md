## Demo web application

**Problem statement**

\_Create web application which helps Rebel Alliance to to display the universe data in the most accessible way so that Admiral Ackbar and Princess Leia can prepare a battle plan to defeat the Galactic Empire. It should provide the data of myriad planets, species, vehicles etc.. The Api documentation of Star wars can be found here - https://swapi.dev/documentation

**Relevent links**
https://swapi.dev/documentation (Star wars api)

**Tech stack**
Web app is designed in React using Typescript

**Tooling**

- Bundeling
  - webpack
- Lint
  - eslint
  - stylelint
  - prettier
- Localization
  - i18next
- Testing
  - jest
    -react-testing-library
- Pre commit hooks
  - husky
- CI / CD
  - gitLab
- Deployment
  - surge

**How to run application**
_Go to the root directory and execute the below command_

```
npm install
npm run start
```

**Pipeline and deployment flow**

- **master branch** - for master the pipeline looks like below as soon as the code is merged to master it will build, test, and deploy the code to integration environment and deploy to production is a manual step. Environment url [Prod](https://rebel-alliance.surge.sh).

```mermaid
graph LR
A((Install)) ---> B((Test)) ---> C((Build)) -- automatic deploy to dev ---> D((deploy))
C -- manual deploy to prod ---> E((deploy)) ---> F((Smoke))
```

- **develop branch** - for develop the pipeline looks like below as soon as the code is merged to develop it will build, test, and deploy the code to integration environment. Environment url [Dev](https://rebel-alliance-dev.surge.sh).

```mermaid
graph LR
A((Install)) ---> B((Test)) ----> C((Build)) -- deploy to dev ---> D((Deploy))
```

- **feature branch** - for feature branches the pipeline looks like below here it will have and extra step 'remove-feature' to remove the application so our systems will not go out of memory it will be executed automatically when the merge request is merged and will remove the app.
  The environment url will be `https://rebel-alliance-<branch-name>.surge.sh`. for example of the branch name is feature/hello then it can be accessed at `https://rebel-alliance-feature-hello.surge.sh`

```mermaid
graph LR
A((Install)) ----> B((Test)) ---> C((Build))  -- automatic ---> D((Deploy))
C -- on merge ---> E((Remove))
```
