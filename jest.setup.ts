import { I18NextTMock, I18NextMock } from '@types';

jest.mock(
  'react-i18next',
  (): I18NextMock => ({
    useTranslation: (): I18NextTMock => ({
      t: (key: string): string => key
    })
  })
);

export {};
