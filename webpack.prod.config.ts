import merge from 'webpack-merge';
import { Configuration } from 'webpack';
import TerserPlugin from 'terser-webpack-plugin';
import path from 'path';
import baseWebpackConfig from './webpack.base.config';

const config: Configuration = merge<Configuration>(baseWebpackConfig, {
  mode: 'production',
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()]
  },
  output: {
    chunkFilename: '[name].[hash].js',
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, 'frontend/dist/prod'),
    publicPath: '/'
  }
});

export default config;
