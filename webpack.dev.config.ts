import merge from 'webpack-merge';
import webpack, { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';
import path from 'path';
import baseWebpackConfig from './webpack.base.config';

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

const config: Configuration = merge<Configuration>(baseWebpackConfig, {
  devServer: {
    client: {
      overlay: false
    },
    historyApiFallback: true,
    hot: true,
    open: true,
    port: 4000,
    static: path.resolve(__dirname, 'frontend/dist/dev')
  },
  devtool: 'source-map',
  mode: 'development',
  output: {
    chunkFilename: '[name].js',
    filename: '[name].js',
    path: path.resolve(__dirname, 'frontend/dist/dev'),
    publicPath: '/'
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
});

export default config;
